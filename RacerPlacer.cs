﻿using GTANetworkAPI;
using ProperCuntingRacingResource.PlacingStates;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProperCuntingRacingResource
{
    public class RacerPlacer
    {
        public abstract class PlacingState
        {
            public RacerPlacer Host { get; set; }
            public Client Player { get; set;  }

            public virtual void EnterState() { }
            public virtual void ExitState() { }
            public virtual void Update() { }
            public virtual void RemoteEvent(Client player, string name, object[] args) { }
            public virtual void Cleanup() { }
        }

        private List<Client> _placingQueue;
        private PlacingState _currentPlacingState;

        private DateTime _lastStateEnter;
        public bool RaceStart;

        public RacerPlacer(bool raceStart)
        {
            _placingQueue = new List<Client>();
            RaceStart = raceStart;
        }

        public int PlayersLoading => _placingQueue.Count;

        public Action<Client> PlayerLoaded { get; set; }
        public Action<Client> PlayerFailedToLoad { get; set; }

        public void EnqueuePlayer(Client player, SpawnPoint sp, int vehicleModel)
        {
            player.SetDataEx("SPAWNPOINT", sp.Position);
            player.SetDataEx("HEADING", sp.Heading);
            player.SetDataEx("VEHICLE_MODEL", vehicleModel);

            if (!_placingQueue.Contains(player))
                _placingQueue.Add(player);
        }

        public void RemovePlayer(Client player)
        {
            _placingQueue.Remove(player);
            if (_currentPlacingState != null && _currentPlacingState.Player == player)
            {
                _currentPlacingState.Cleanup();
                _currentPlacingState = null;
            }
        }

        public void TransitionState(PlacingState newState)
        {
            _currentPlacingState?.ExitState();


            _currentPlacingState = newState;
            if (newState != null)
            {
                Console.WriteLine("[{1}] ({2}) Transitioning to state {0}", _currentPlacingState.GetType().Name, DateTime.Now.ToLongTimeString(), _placingQueue[0].Name);
                _lastStateEnter = DateTime.UtcNow;
                _currentPlacingState.Player = _placingQueue[0];
                _currentPlacingState.Host = this;
                _currentPlacingState.EnterState();
            }
        }

        public void FinishStateMachine(PlacingState lastState)
        {
            if (lastState != null &&
                lastState == _currentPlacingState &&
                lastState.Player == _placingQueue[0])
            {
                PlayerLoaded?.Invoke(_placingQueue[0]);

                lastState.ExitState();
                _placingQueue.RemoveAt(0);
                _currentPlacingState = null;
            }
        }

        public void Update()
        {
            if (_currentPlacingState != null)
            {
                _currentPlacingState.Update();

                if ((DateTime.UtcNow - _lastStateEnter).TotalSeconds > 20)
                {
                    Client c = _placingQueue[0];
                    Console.WriteLine("[{0}] Player {1} did not load in time.", DateTime.Now.ToLongTimeString(), c.Name);
                    string statename = _currentPlacingState.GetType().Name;
                    RemovePlayer(c);
                    PlayerFailedToLoad?.Invoke(c);
                    if (RaceStart) c.Kick("Did not load in time. Loading state: " + statename);
                }
            }
            else if (_placingQueue.Count > 0)
            {
                TransitionState(new InitPlacingState());                
            }
        }

        public void RemoteEvent(Client player, string name, object[] args)
        {
            _currentPlacingState?.RemoteEvent(player, name, args);
        }
    }
}
