using System;
using System.Collections.Generic;
using System.Linq;
using GTANetworkAPI;
using ProperCuntingRacingResource.States;

namespace ProperCuntingRacingResource
{
    public class Master : Script
    {

        public Master()
        {
            NAPI.Server.SetAutoSpawnOnConnect(false);
            NAPI.Server.SetAutoRespawnAfterDeath(false);
            DatabaseContext = new MapRatingDb();
            DatabaseContext.EnsureCreated();

            RaceParser.DiscoverMaps();
            TransitionState(new PreGameState());

        }

        private static GameState _currentState;
        public static MapRatingDb DatabaseContext;

        public static void TransitionState(GameState nextState)
        {
            if (nextState == null)
                throw new ArgumentNullException(nameof(nextState));

            Console.WriteLine("[RACE] Changing state from {0} to {1}", _currentState?.StateName, nextState.StateName);

            if (_currentState != null)
                _currentState.LeaveState(nextState);

            nextState.EnterState(_currentState);

            _currentState = nextState;
        }

        private static T[] SubArray<T>(T[] data, int index, int length)
        {
            T[] result = new T[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }

        private static T[] SubArray<T>(T[] data, int index)
        {
            int length = data.Length - index;
            T[] result = new T[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }


        [RemoteEvent("RACE_REMOTE_EVENT")]
        public void RaceRemoteEvent(Client player, params object[] args)
        {
            if (args.Length < 1) return;
            string realName = (string)args[0];
            _currentState?.OnRemoteEvent(player, realName, SubArray(args, 1));
        }

        /* COMMANDS */

        [Command(GreedyArg =true)]
        public void ForceMap(Client p, string map)
        {
            if (p.Name != "Guad") return; // dont do this
            map = System.IO.Path.Combine("race-maps", map);
            TransitionState(new RaceCleanupState(RaceParser.Parse(map)));
        }

        ///

        [ServerEvent(Event.Update)]
        public  void OnUpdate()
        {
            _currentState?.OnUpdate();
        }

        [ServerEvent(Event.ChatMessage)]
        public  void OnChatMessage(Client player, string message)
        {
            _currentState?.OnChatMessage(player, message);
        }

        [ServerEvent(Event.PlayerConnected)]
        public  void OnPlayerConnected(Client player)
        {
            _currentState?.OnPlayerConnected(player);
        }

        [ServerEvent(Event.PlayerDisconnected)]
        public  void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            _currentState?.OnPlayerDisconnected(player, type, reason);
            DataExtension.CleanEntity(player);
        }
        
        [ServerEvent(Event.PlayerDeath)]
        public  void OnPlayerDeath(Client player, Client killer, uint reason)
        {
            _currentState?.OnPlayerDeath(player, killer, reason);
        }

        [ServerEvent(Event.PlayerSpawn)]
        public  void OnPlayerSpawn(Client player)
        {
            _currentState?.OnPlayerSpawn(player);
        }

        [ServerEvent(Event.PlayerPickup)]
        public  void OnPlayerPickup(Client player, Pickup pickup)
        {
            _currentState?.OnPlayerPickup(player, pickup);
        }

        [ServerEvent(Event.PlayerDamage)]
        public  void OnPlayerDamage(Client player, float healthloss, float armorloss)
        {
            _currentState?.OnPlayerDamage(player, healthloss, armorloss);
        }

        [ServerEvent(Event.PlayerEnterVehicle)]
        public void OnPlayerEnterVehicle(Client player, Vehicle vehicle, sbyte seatId)
        {
            _currentState?.OnPlayerEnterVehicle(player, vehicle, seatId);
        }

        [ServerEvent(Event.PlayerExitVehicle)]
        public  void OnPlayerExitVehicle(Client player, Vehicle vehicle)
        {
            _currentState?.OnPlayerExitVehicle(player, vehicle);
        }

        [ServerEvent(Event.PlayerWeaponSwitch)]
        public  void OnPlayerWeaponSwitch(Client player, WeaponHash oldWeapon, WeaponHash newWeapon)
        {
            _currentState?.OnPlayerWeaponSwitch(player, oldWeapon, newWeapon);
        }

        [ServerEvent(Event.PlayerDetonateStickies)]
        public  void OnPlayerDetonateStickies(Client player)
        {
            _currentState?.OnPlayerDetonateStickies(player);
        }

        [ServerEvent(Event.PlayerEnterColshape)]
        public  void OnPlayerEnterColShape(ColShape shape, Client player)
        {
            _currentState?.OnPlayerEnterColShape(shape, player);
        }

        [ServerEvent(Event.PlayerExitColshape)]
        public  void OnPlayerExitColShape(ColShape shape, Client player)
        {
            _currentState?.OnPlayerExitColShape(shape, player);
        }

        [ServerEvent(Event.PlayerEnterCheckpoint)]
        public  void OnPlayerEnterCheckpoint(Checkpoint checkpoint, Client player)
        {
            _currentState?.OnPlayerEnterCheckpoint(checkpoint, player);
        }

        [ServerEvent(Event.PlayerExitCheckpoint)]
        public  void OnPlayerExitCheckpoint(Checkpoint checkpoint, Client player)
        {
            _currentState?.OnPlayerExitCheckpoint(checkpoint, player);
        }

        [ServerEvent(Event.VehicleDamage)]
        public  void OnVehicleDamage(Vehicle vehicle, float bodyHealthLoss, float engineHealthLoss)
        {
            _currentState?.OnVehicleDamage(vehicle, bodyHealthLoss, engineHealthLoss);
        }

        [ServerEvent(Event.VehicleTyreBurst)]
        public  void OnVehicleTyreBurst(Vehicle vehicle, int tyreIndex)
        {
            _currentState?.OnVehicleTyreBurst(vehicle, tyreIndex);
        }

        [ServerEvent(Event.VehicleDeath)]
        public  void OnVehicleDeath(Vehicle veh)
        {
            _currentState?.OnVehicleDeath(veh);
        }

    }
}