﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProperCuntingRacingResource
{
    public static class Checkpoints
    {
        public class Checkpoint
        {
            public Vector3 Position;
            public float Size;
            public int Id;
            public int? TransformVehicle;
            public bool Secondary;

            public float SizeSquared;
        }

        private static Dictionary<int, List<Checkpoint>> _checkpoints = new Dictionary<int, List<Checkpoint>>();

        public static Action<Client, Checkpoint> OnPlayerInsideCheckpoint;

        public static void Add(Checkpoint cp)
        {
            if (!_checkpoints.ContainsKey(cp.Id))
                _checkpoints.Add(cp.Id, new List<Checkpoint>());

            cp.SizeSquared = cp.Size * cp.Size;

            _checkpoints[cp.Id].Add(cp);
        }

        public static void Clean()
        {
            _checkpoints.Clear();
        }

        public static void Update()
        {
            var players = NAPI.Pools.GetAllPlayers();

            foreach (var p in players)
            {
                if (!p.HasDataEx("CHECKPOINTS_PASSED")) continue;
                int chk = (int) p.GetDataEx("CHECKPOINTS_PASSED");

                if (!_checkpoints.ContainsKey(chk)) continue;

                foreach (var c in _checkpoints[chk])
                {
                    if ((c.Position - p.Position).LengthSquared() < c.SizeSquared)
                    {
                        OnPlayerInsideCheckpoint?.Invoke(p, c);
                    }
                }
            }
        }
    }
}
