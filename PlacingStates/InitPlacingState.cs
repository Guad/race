﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProperCuntingRacingResource.PlacingStates
{
    public class InitPlacingState : RacerPlacer.PlacingState
    {
        private Util.Delayer _delayer = new Util.Delayer();
        public override void EnterState()
        {
            Vector3 spawnpoint = (Vector3) Player.GetDataEx("SPAWNPOINT");

            Player.SetDataEx("HAS_LOADED", false);
            Player.SetDataEx("HAS_BEEN_PLACED", true);

            Util.FreezeEntityForPlayer(Player, Player, true);

            NAPI.Player.SpawnPlayer(Player, spawnpoint);

            if (Player.HasDataEx("RACE_VEHICLE") && Player.GetDataEx("RACE_VEHICLE") != null)
            {
                Console.WriteLine("Player had a car. Deleting.");
                Vehicle car = (Vehicle)Player.GetDataEx("RACE_VEHICLE");
                car.Delete();
                DataExtension.CleanEntity(car);
                Player.ResetDataEx("RACE_VEHICLE");
            }
            else
                Console.WriteLine("Play had no car.");

            _delayer.DelayAction(500, () =>
            {
                Host.TransitionState(new ModelRequestState());
            });
        }

        public override void Update()
        {
            _delayer.Update();
        }
    }
}
