﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProperCuntingRacingResource.PlacingStates
{
    public class VehiclePlacingState : RacerPlacer.PlacingState
    {
        private Util.Delayer _delayer = new Util.Delayer();
        private DateTime? _lastVehCheckSent;
        private Vehicle _playersCar;

        public override void EnterState()
        {
            Vector3 spawnpoint = (Vector3)Player.GetDataEx("SPAWNPOINT");
            float heading = (float) Player.GetDataEx("HEADING");
            int model = (int) Player.GetDataEx("VEHICLE_MODEL");            

            int color1 = Util.GetRandInt(100);
            int color2 = Util.GetRandInt(100);

            if (Player.HasDataEx("VEHICLE_COL1"))
            {
                color1 = (int)Player.GetDataEx("VEHICLE_COL1");
                color2 = (int)Player.GetDataEx("VEHICLE_COL2");

                Console.WriteLine("Player had colors: {0}, {1}", color1, color2);
            }
            else
            {
                Player.SetDataEx("VEHICLE_COL1", color1);
                Player.SetDataEx("VEHICLE_COL2", color2);
            }

            _playersCar = NAPI.Vehicle.CreateVehicle(model, spawnpoint, heading, color1, color2,
                    locked: true, engine: true);

            Player.SetDataEx("RACE_VEHICLE", _playersCar);
            Player.TriggerEvent("CHECK_VEHICLE_PLACEMENT", _playersCar);
            _lastVehCheckSent = DateTime.UtcNow;
        }

        public override void Update()
        {
            _delayer.Update();

            if (_lastVehCheckSent.HasValue && (DateTime.UtcNow - _lastVehCheckSent.Value).TotalSeconds > 5)
            {
                Player.TriggerEvent("CHECK_VEHICLE_PLACEMENT", _playersCar);
                _lastVehCheckSent = DateTime.UtcNow;
            }
        }

        public override void RemoteEvent(Client player, string name, object[] args)
        {
            if (player != Player) return;

            if (name == "VEHICLE_PLACEMENT_OK")
            {
                Host.TransitionState(new PlayerPlacingState(_playersCar));
            }
            else if (name == "RESEND_VEHICLE_PLACEMENT")
            {
                player.TriggerEvent("CHECK_VEHICLE_PLACEMENT", _playersCar);
            }
        }

        public override void Cleanup()
        {
            _playersCar.Delete();
        }
    }
}
