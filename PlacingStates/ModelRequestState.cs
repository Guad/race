﻿using System;
using System.Collections.Generic;
using System.Text;
using GTANetworkAPI;

namespace ProperCuntingRacingResource.PlacingStates
{
    public class ModelRequestState : RacerPlacer.PlacingState
    {
        private DateTime? _lastVehCheckSent;
        private int _vehmodel;
        public override void EnterState()
        {
            Vector3 spawnpoint = (Vector3)Player.GetDataEx("SPAWNPOINT");
            Player.Position = spawnpoint;
            _vehmodel = (int) Player.GetDataEx("VEHICLE_MODEL");
            _lastVehCheckSent = DateTime.UtcNow;
            Player.TriggerEvent("REQUEST_MODEL", _vehmodel);
        }

        public override void RemoteEvent(Client player, string name, object[] args)
        {
            if (player != Player) return;
            if (name == "MODEL_LOADED")
            {
                Host.TransitionState(new VehiclePlacingState());
            }
        }

        public override void Update()
        {
            if (_lastVehCheckSent.HasValue && (DateTime.UtcNow - _lastVehCheckSent.Value).TotalSeconds > 3)
            {
                Player.TriggerEvent("REQUEST_MODEL", _vehmodel);
                _lastVehCheckSent = DateTime.UtcNow;
            }
        }
    }
}
