﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProperCuntingRacingResource.PlacingStates
{
    public class PlayerPlacingState : RacerPlacer.PlacingState
    {
        private Util.Delayer _delayer = new Util.Delayer();
        private Vehicle _playersCar;
        public PlayerPlacingState(Vehicle car)
        {
            _playersCar = car;
        }

        public override void EnterState()
        {

            string plate = Player.Name;

            if (plate.Length > 9)
                plate = plate.Substring(0, 9);

            _playersCar.NumberPlate = plate;
            _delayer.DelayAction(300, () =>
            {
                Util.FreezeEntityForPlayer(Player, _playersCar, true);
                Player.TriggerEvent("SET_ON_GROUND", _playersCar);
            });

            _delayer.DelayAction(1000, () =>
            {
                Player.SetIntoVehicle(_playersCar, -1);

                Util.FreezeEntityForPlayer(Player, _playersCar, Host.RaceStart);

                Player.SetDataEx("HAS_BEEN_PLACED", true);
                Player.SetDataEx("HAS_LOADED", true);
                Player.SetDataEx("IN_RACE", true);                

                if (!Player.HasDataEx("CHECKPOINTS_PASSED")) Player.SetDataEx("CHECKPOINTS_PASSED", 0);
                Host.FinishStateMachine(this);
            });
        }

        public override void Update()
        {
            _delayer.Update();
        }

        public override void Cleanup()
        {
            _playersCar.Delete();
        }
    }
}
