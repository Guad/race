﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Data.Sqlite;

namespace ProperCuntingRacingResource
{
    public class MapRatingDb
    {
        private const string ConnectionString = "DataSource=map_ratings.db";

        public void EnsureCreated()
        {
            try
            {
                using (var connection = new SqliteConnection(ConnectionString))
                {
                    connection.Open();

                    var tableCommand = connection.CreateCommand();

                    tableCommand.CommandText = "CREATE TABLE IF NOT EXISTS ratings (filename text PRIMARY KEY, likes integer, dislikes integer);";

                    tableCommand.ExecuteNonQuery();
                }
            }
            catch(SqliteException)
            {
                //throw;
            }
        }

        public bool GetMapRating(string mapFilename, out int likes, out int dislikes)
        {
            mapFilename = System.IO.Path.GetFileName(mapFilename);

            try
            {
                using (var connection = new SqliteConnection(ConnectionString))
                {
                    connection.Open();

                    var selectCommand = connection.CreateCommand();
                    selectCommand.CommandText = "SELECT likes, dislikes FROM ratings WHERE filename = $fn";
                    selectCommand.Parameters.AddWithValue("$fn", mapFilename);
                    using (var reader = selectCommand.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            likes = reader.GetInt32(0);
                            dislikes = reader.GetInt32(1);
                            return true;
                        }
                        else
                        {
                            likes = 0;
                            dislikes = 0;
                            return false;
                        }

                    }
                }
            }
            catch(SqliteException)
            {
                likes = 0;
                dislikes = 0;
                return false;
            }
        }

        public void UpdateMapRating(string mapfilename, int addLikes, int addDislikes)
        {
            mapfilename = System.IO.Path.GetFileName(mapfilename);

            int currentLikes, currentDislikes;

            bool exists = GetMapRating(mapfilename, out currentLikes, out currentDislikes);

            try
            {
                using (var connection = new SqliteConnection(ConnectionString))
                {
                    connection.Open();

                    var modifyCommand = connection.CreateCommand();
                    modifyCommand.CommandText = exists ? "UPDATE ratings SET likes = $likes, dislikes = $dislikes WHERE filename = $fn" :
                                                        "INSERT INTO ratings (filename, likes, dislikes) VALUES ($fn, $likes, $dislikes)";

                    modifyCommand.Parameters.AddWithValue("$fn", mapfilename);
                    modifyCommand.Parameters.AddWithValue("likes", currentLikes + addLikes);
                    modifyCommand.Parameters.AddWithValue("$dislikes", currentDislikes + addDislikes);

                    modifyCommand.ExecuteNonQuery();
                }
            }
            catch (SqliteException)
            {
            }
        }
    }
}
