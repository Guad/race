﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProperCuntingRacingResource
{
    public static class DataExtension
    {
        private static Dictionary<Entity, Dictionary<string, object>> _datas = new Dictionary<Entity, Dictionary<string, object>>();

        public static void SetDataEx(this Entity ent, string key, object data)
        {
            if (!_datas.ContainsKey(ent)) _datas.Add(ent, new Dictionary<string, object>());

            if (_datas[ent].ContainsKey(key)) _datas[ent][key] = data;
            else _datas[ent].Add(key, data);
        }

        public static object GetDataEx(this Entity ent, string key)
        {
            if (!_datas.ContainsKey(ent)) return null;
            if (_datas[ent].ContainsKey(key)) return _datas[ent][key];
            return null;
        }

        public static bool HasDataEx(this Entity ent, string key)
        {
            if (!_datas.ContainsKey(ent)) return false;
            return _datas[ent].ContainsKey(key);
        }

        public static void ResetDataEx(this Entity ent, string key)
        {
            if (!_datas.ContainsKey(ent)) return;
            _datas[ent].Remove(key);
        }

        public static void Clean()
        {
            _datas.Clear();
        }
        public static void CleanEntity(Entity ent)
        {
            _datas.Remove(ent);
        }
    }
}
