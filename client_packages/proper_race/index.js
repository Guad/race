﻿mp.nametags.set(
{
    font: 6,
    outline: true,
    offset: 0.6,
    veh_offset: 1.0,
    color: [255, 255, 255, 255],
    size: 0.5,

    hbar:
        {
            size: [0.06, 0.012],
            color: [115, 0, 0, 0],
            bg_color: [0, 0, 0, 0],
            offset: 0.06,
            border: [0.0012, 0.0012 * 1.5]
        },
    });


chat = function (str) {
    mp.gui.chat.push(str);
};
/* OTHER PLAYERS */

// entity-blip dict
let playerBlips = [];

// unused blips
let blipPool = [];

mp.events.add('entityStreamIn', (entity) => {

    if (entity != null && entity != undefined && entity.isAPed()) {
        if (blipPool.length == 0) {
            let blip = mp.blips.new(1, entity.position, {
                name: "Checkpoint",
                scale: 0.7,
                color: 0,
                alpha: 255,
                drawDistance: 100,
                shortRange: false,
                rotation: 0,
                dimension: 0,
            });

            playerBlips.push([entity, blip]);
        } else {
            let blip = blipPool[0];
            blipPool.splice(0, 1);
            blip.setAlpha(255);
            blip.setCoords(entity.position);
            playerBlips.push([entity, blip]);
        }
    }
});
mp.events.add('render', () => {
    let cleanup = [];

    playerBlips.forEach((val, i, arr) => {
        let entity = val[0];
        let blip = val[1];

        //chat('Key: ' + entity + ' type: ' + typeof (entity));
        try {
            if (!entity.doesExist()) {
                cleanup.push(i);
                blip.setAlpha(0);
                blipPool.push(blip);
            } else {
                blip.setCoords(entity.position);
            }
        } catch (e) {
            cleanup.push(i);
            blip.setAlpha(0);
            blipPool.push(blip);
        }
    });

    cleanup.forEach((el, indx, arr) => {
        playerBlips.splice(el, 1);
    });

});

let countdownScaleformId = mp.game.graphics.requestScaleformMovie('countdown');
let renderCountdown = false;

mp.game.gameplay.setFadeOutAfterDeath(false);

mp.game.audio.requestScriptAudioBank("HUD_MINI_GAME_SOUNDSET");

if (!mp.game.graphics.hasNamedScaleformMovieLoaded("countdown")) {
    countdownScaleformId = mp.game.graphics.requestScaleformMovie('countdown');
}

mp.events.add({
    'FREEZE_ENTITY': (ent, freeze) => {
        //chat('[CLIENT]: Received FREEZE_ENTITY event with param ' + ent + ' ' + freeze);
        if (ent != null && ent != undefined) {
            ent.freezePosition(freeze);
        }
    }
});


/* RACE LOADING STATE */

let objectPlacementCheck = null;
let vehiclePlacementCheck = null;
let instructionalButtonsScaleform = mp.game.graphics.requestScaleformMovie('instructional_buttons');
let renderButtons = false;
let buttons = [];
let vehicleModels = [];

mp.events.add('render', () => {
    if (renderButtons) {
        if (buttons.length > 0) updateInstructionalButtons();
        mp.game.graphics.drawScaleformMovieFullscreen(instructionalButtonsScaleform, 255, 255, 255, 255, false);
    }

    if (vehiclePlacementCheck != null && vehiclePlacementCheck.handle != 0) {
        vehiclePlacementCheck = null;
        mp.events.callRemote("RACE_REMOTE_EVENT", "VEHICLE_PLACEMENT_OK");
    }

    if (objectPlacementCheck != null && objectPlacementCheck.handle != 0) {
        objectPlacementCheck = null;
        mp.events.callRemote("RACE_REMOTE_EVENT", "OBJECT_PLACEMENT_OK");
    }
});

mp.events.add('CHECK_OBJECT_PLACEMENT', (objectInQuestion) => {
    if (objectInQuestion == null) {
        mp.events.callRemote("RACE_REMOTE_EVENT", "RESEND_OBJECT_PLACEMENT");
        return;
    }

    if (objectInQuestion.handle == 0) {
        mp.events.callRemote("RACE_REMOTE_EVENT", "OBJECT_PLACEMENT_OK");
    } else {
        objectPlacementCheck = objectInQuestion;
    }
});

mp.events.add('CHECK_VEHICLE_PLACEMENT', (vehicleInQuestion) => {
    if (vehicleInQuestion == null) {
        mp.events.callRemote("RACE_REMOTE_EVENT", "RESEND_VEHICLE_PLACEMENT");
        return;
    }

    if (vehicleInQuestion.handle == 0) {
        mp.events.callRemote("RACE_REMOTE_EVENT", "VEHICLE_PLACEMENT_OK");
    } else {
        vehiclePlacementCheck = vehicleInQuestion;
    }
});

mp.events.add('CONFIRM_IN_VEHICLE', (v) => {
    let p = mp.players.local;
    let currentV = p.getVehicleIsUsing();
    if (v != null && v != undefined && currentV != null && currentV != undefined && currentV != 0 && v.handle == currentV) {
        mp.events.callRemote("RACE_REMOTE_EVENT", "CONFIRM_SET_IN_VEHICLE");
    } else {
        mp.events.callRemote("RACE_REMOTE_EVENT", "REJECT_SET_IN_VEHICLE");
    }
});

mp.events.add('SET_INSTRUCTIONAL_BUTTONS', (json) => {
    buttons = JSON.parse(json);
    updateInstructionalButtons();
    renderButtons = true;
});

mp.events.add('SET_LOADING_TEXT', (text) => {
    buttons = [];
    setLoadingText(text);
    renderButtons = true;
});

mp.events.add('HIDE_INSTRUCTIONAL_BUTTONS', (json) => {
    renderButtons = false;
});

mp.events.add('REQUEST_VEHICLES', (json) => {
    let models = JSON.parse(json);
    vehicleModels = models;

    models.forEach((v, i, arr) => {
        mp.game.streaming.requestModel(v);
    });
});

mp.events.add('CLEAR_REQUESTED_VEHICLES', (json) => {
    vehicleModels.forEach((v, i, arr) => {
        mp.game.streaming.setModelAsNoLongerNeeded(v);
    });
    
    vehicleModels = [];
});

mp.events.add('REQUEST_MODEL', (model) => {
    if (mp.game.streaming.hasModelLoaded(model)) {
        mp.events.callRemote("RACE_REMOTE_EVENT", "MODEL_LOADED");
    } else {
        mp.game.streaming.requestModel(model);

        let loadingLoop = setInterval(() => {
            if (mp.game.streaming.hasModelLoaded(model)) {
                clearInterval(loadingLoop);
                mp.events.callRemote("RACE_REMOTE_EVENT", "MODEL_LOADED");
            } else {
                mp.game.streaming.requestModel(model);
            }
        }, 33);
    }
});

mp.events.add('SET_ON_GROUND', (v) => {
    if (v != null && v != undefined) {
        v.setOnGroundProperly();
    }
});


function setLoadingText(text) {
    callFunctionHead(instructionalButtonsScaleform, "CLEAR_ALL");
    callFunctionEnd();

    callFunctionHead(instructionalButtonsScaleform, "TOGGLE_MOUSE_BUTTONS");
    pushInt(0);
    callFunctionEnd();

    callFunctionHead(instructionalButtonsScaleform, "CREATE_CONTAINER");
    callFunctionEnd();

    callFunctionHead(instructionalButtonsScaleform, "SET_DATA_SLOT");
    pushInt(0);
    pushInt(50);
    pushString(text);
    callFunctionEnd();

    callFunctionHead(instructionalButtonsScaleform, "DRAW_INSTRUCTIONAL_BUTTONS");
    pushInt(-1);
    callFunctionEnd();
}

function updateInstructionalButtons() {
    callFunctionHead(instructionalButtonsScaleform, "CLEAR_ALL");
    callFunctionEnd();

    callFunctionHead(instructionalButtonsScaleform, "TOGGLE_MOUSE_BUTTONS");
    pushInt(0);
    callFunctionEnd();

    callFunctionHead(instructionalButtonsScaleform, "CREATE_CONTAINER");
    callFunctionEnd();

    // Put buttons here
    buttons.forEach((element, index, arr) => {
        callFunctionHead(instructionalButtonsScaleform, "SET_DATA_SLOT");
        pushInt(index);
        pushString(mp.game.controls.getControlActionName(2, element[0], false));
        pushString(element[1]);
        callFunctionEnd();
    });

    callFunctionHead(instructionalButtonsScaleform, "DRAW_INSTRUCTIONAL_BUTTONS");
    pushInt(-1);
    callFunctionEnd();
}

/* COUNTDOWN STATE */


let raceBanner1 = mp.game.graphics.requestScaleformMovie('mp_celebration_bg');
let raceBanner2 = mp.game.graphics.requestScaleformMovie('mp_celebration_fg');
let raceBanner3 = mp.game.graphics.requestScaleformMovie('mp_celebration');

let renderBanners = false;

function createIntroBanner(test, racename) {
    let var0 = "intro";
    let var1 = "HUD_COLOUR_BLACK";
    let var2 = ""
    let var3 = false;

    callFunctionHead(test, "CLEANUP");
    pushString(var0);
    callFunctionEnd();

    callFunctionHead(test, "CREATE_STAT_WALL");
    pushString(var0);
    pushString(var1);
    callFunctionEnd();

    callFunctionHead(test, "SET_PAUSE_DURATION");
    pushInt(5); // CHANGEME
    callFunctionEnd();

    callFunctionHead(test, "ADD_INTRO_TO_WALL");
    pushString(var0);
    pushString(var2);
    pushString(racename);
    pushString(" ");// celeb challenge
    pushString("RACE"); // <= 1

    pushString(" ");
    pushString(" ");
    pushInt(0);
    pushInt(0);
    pushString(" ");
    pushBool(false);
    pushString("HUD_COLOUR_WHITE");

    callFunctionEnd();
    // ADD_BACKGROUND
    callFunctionHead(test, "ADD_BACKGROUND_TO_WALL");
    pushString(var0);
    pushInt(75);

    pushInt(3); // 5, 3, 1, 6
    callFunctionEnd();

    callFunctionHead(test, "SHOW_STAT_WALL");
    pushString(var0);
    callFunctionEnd();
}


mp.events.add('render', () => {
    if (renderBanners) {
        mp.game.graphics.drawScaleformMovieFullscreen(raceBanner1, 255, 255, 255, 255, false);
        mp.game.graphics.drawScaleformMovieFullscreen(raceBanner2, 255, 255, 255, 255, false);
        mp.game.graphics.drawScaleformMovieFullscreen(raceBanner3, 255, 255, 255, 255, false);
    }

    if (renderCountdown) {
        mp.game.graphics.drawScaleformMovieFullscreen(countdownScaleformId, 255, 255, 255, 255, false);
    }
});


mp.events.add('RACE_START_COUNTDOWN', (name) => {
    // 10 serconds total
    // 7 seconds nothing
    // then 1 second per number 

    createIntroBanner(raceBanner1, name);
    createIntroBanner(raceBanner2, name);
    createIntroBanner(raceBanner3, name);

    renderBanners = true;

    setTimeout(() => {    
        renderCountdown = true;

        // 3
        playSound("HUD_MINI_GAME_SOUNDSET", "CHECKPOINT_NORMAL");

        callFunctionHead(countdownScaleformId, "FADE_MP");
        pushInt(3);
        pushInt(241);
        pushInt(247);
        pushInt(57);
        callFunctionEnd();
    }, 7000);

    // 2
    setTimeout(() => {
        playSound("HUD_MINI_GAME_SOUNDSET", "CHECKPOINT_NORMAL");

        callFunctionHead(countdownScaleformId, "FADE_MP");
        pushInt(2);
        pushInt(241);
        pushInt(247);
        pushInt(57);
        callFunctionEnd();
    }, 8000);

    // 1
    setTimeout(() => {
        playSound("HUD_MINI_GAME_SOUNDSET", "CHECKPOINT_NORMAL");

        callFunctionHead(countdownScaleformId, "FADE_MP");
        pushInt(1);
        pushInt(241);
        pushInt(247);
        pushInt(57);
        callFunctionEnd();
    }, 9000);

    // go
    setTimeout(() => {
        playSound("DLC_HEIST_HACKING_SNAKE_SOUNDS", "Beep_Green");

        callFunctionHead(countdownScaleformId, "FADE_MP");
        pushString("go");
        pushInt(49);
        pushInt(235);
        pushInt(126);
        callFunctionEnd();
    }, 10000);

    // remove
    setTimeout(() => {
        renderCountdown = false;
        renderBanners = false;
    }, 14000);
});


/* RACE STATE */

let currentRaceBlip = null;
let nextRaceBlip = null;

let currentSecondaryRaceBlip = null;
let nextSecondaryRaceBlip = null;

let checkpointData = null;

let uiBrowser = mp.browsers.new("package://proper_race/ui.html");
uiBrowser.active = false;

const maxRespawnTime = 3000;
let requestRespawn = 0;
let requestSkip = 0;
let lastVehicle = null;

let nitroAmount = 0;
let nitroStart = 0;
const totalNitro = 10000;

let activateNitro = false;

let vehiclesWithNitro = [];

let ghostVehicles = [];

let exhausts = ["exhaust", "exhaust_2", "exhaust_4", "exhaust_5", "exhaust_6", "exhaust_7"];

let savingVelocity = true;
let lastVelocity = null;

function startNitro() {
    activateNitro = true;

    let lp = mp.players.local;
    let v = lp.getVehicleIsUsing();
    if (v != null) {
        mp.events.callRemote("RACE_REMOTE_EVENT", "START_NITRO", mp.vehicles.atHandle(v));
        mp.game.invoke('0x93A3996368C94158', v, 100.1);
    }
}

function stopNitro() {
    activateNitro = false;

    let lp = mp.players.local;
    let v = lp.getVehicleIsUsing();
    if (v != null) {
        mp.events.callRemote("RACE_REMOTE_EVENT", "STOP_NITRO", mp.vehicles.atHandle(v));
        mp.game.invoke('0x93A3996368C94158', v, 0);
    }
}

function setVehicleGhost(vehicle, ghost) {
    let lp = mp.players.local;
    let vh = lp.getVehicleIsUsing();

    if (vh == 0 || vh == null || vehicle == null || vehicle == undefined) return;

    try {
        vehicle.setNoCollision(vh, !ghost);
        if (ghost) vehicle.setAlpha(100);
        else vehicle.resetAlpha();
    } catch (err) {

    }

}

function updateGhostVehicles() {
    ghostVehicles.forEach((v, i, arr) => {
        setVehicleGhost(v, true);
    });
}

let screenFaded = false;
let fadingStart = 0;
const fadingLen = 100;

mp.events.add('render', () => {
    if (fadingStart != 0) {
        let delta = Date.now() - fadingStart;
        let percent = delta / fadingLen;

        if (percent > 1) {
            fadingStart = 0;
            return;
        }

        if (screenFaded) {
            mp.game.graphics.drawRect(0.5, 0.5, 1, 1,
                0, 0, 0, 255 * percent);
        } else {
            mp.game.graphics.drawRect(0.5, 0.5, 1, 1,
                0, 0, 0, 255 * (1 - percent));
        }
    } else if (screenFaded) {
        mp.game.graphics.drawRect(0.5, 0.5, 1, 1,
            0, 0, 0, 255);
    }
    
});

mp.events.add('FADE_SCREEN', (fade) => {
    screenFaded = !fade;
    fadingStart = Date.now();
});

mp.events.add('START_VEHICLE_NITRO_EFFECT', (v) => {
    if (v != null && v.handle != 0) {
        vehiclesWithNitro.push(v);
    }
});

mp.events.add('STOP_VEHICLE_NITRO_EFFECT', (v) => {
    let indx = vehiclesWithNitro.indexOf(v);
    if (indx != -1) {
        vehiclesWithNitro.splice(indx, 1);
    }
});

mp.events.add('GIVE_NITRO', (am) => {
    nitroAmount = Math.min(nitroAmount + am, totalNitro);
    uiBrowser.execute(`setTurbo(${nitroAmount / totalNitro});`);
});

mp.events.add('SET_BLADES_FULLSPEED', (v) => {
    if (v != undefined && v != null) {
        mp.game.invoke('0xA178472EBB8AE60D', v);
        v.applyForceToCenterOfMass(
            1, // force type
            0, 100.5, 0, // offset
            false, // p5
            true, // isDirectionRel
            true, // isForceRel
            false); // p8
    }
});

mp.events.add('SAVE_VELOCITY', (v) => {
    savingVelocity = false;
});

mp.events.add('APPLY_VELOCITY', (v) => {
    if (v != undefined && v != null && lastVelocity != null) {
        v.setVelocity(lastVelocity.x, lastVelocity.y, lastVelocity.z);
        lastVelocity = null;
    } else {
        chat("SAVE_VELOCITY: car or lastVelocity was null");
    }
    savingVelocity = true;
});

mp.events.add('SET_VEHICLE_GHOSTMODE', (v, ghost) => {
    if (v != null && v.handle != 0) {
        let lp = mp.players.local;
        let vh = lp.getVehicleIsUsing();

        if (vh == v.handle) return;

        if (ghost) {
            ghostVehicles.push(v);            
        } else {
            let indx = ghostVehicles.indexOf(v);
            if (indx != -1) ghostVehicles.splice(indx, 1);
        }

        setVehicleGhost(v, ghost);
    }
});

mp.events.add('UPDATE_VEHICLE_GHOSTMODES', () => {
    updateGhostVehicles();
});

function finishCreatingFx(library, name, pos, size) {
    mp.game.invoke('0x6C38AF3693A69A91', library); // USE_PARTICLE_FX_ASSET
    //mp.game.graphics.setParticleFxNonLoopedAlpha(0.05);
    mp.game.graphics.startParticleFxNonLoopedAtCoord(name, pos.x, pos.y, pos.z,
        0, 0, 0, size, false, false, false);
}


mp.events.add('CREATE_FX', (library, name, pos, size) => {
    if (mp.game.invoke('0x8702416E512EC454', library)) {
        finishCreatingFx(library, name, pos, size);
    } else {
        mp.game.invoke('0xB80D8756B4668AB6', library);
        let loadingLoop = setInterval(() => {
            if (mp.game.invoke('0x8702416E512EC454', library)) {
                clearInterval(loadingLoop);
                finishCreatingFx(library, name, pos, size);
            } else {
                mp.game.invoke('0xB80D8756B4668AB6', library);
            }
        }, 33);
    }
});


function drawCheckpoints() {
    let sizeMod = checkpointData.size;
    let debugInfo = null;

    let currentCheckpointPos = checkpointData.current;
    let nextCheckpointPos = checkpointData.next;

    mp.game.graphics.drawMarker(checkpointData.currentFinish ? 4 : 1,
        currentCheckpointPos.x, currentCheckpointPos.y, currentCheckpointPos.z, // pos
        0, 0, 0, // dir
        0, 0, 0, // rot
        10 * sizeMod, 10 * sizeMod, 10 * sizeMod, // scale
        255, 229, 0, 100, // r g b a
        false, // bobUpAndDown
        checkpointData.currentFinish, // FaceCamera
        2,
        false, "", "", false
    );

    if (checkpointData.secondaryCurrent != null) {
        mp.game.graphics.drawMarker(
            1,
            checkpointData.secondaryCurrent.x, checkpointData.secondaryCurrent.y, checkpointData.secondaryCurrent.z, // pos
            0, 0, 0, // dir
            0, 0, 0, // rot
            10 * sizeMod, 10 * sizeMod, 10 * sizeMod, // scale
            255, 140, 0, 100, // r g b a
            false, false, 2,
            false, "", "", false
        );
    }

    if (nextCheckpointPos != null) {
        let x = currentCheckpointPos.x - nextCheckpointPos.x;
        let y = currentCheckpointPos.y - nextCheckpointPos.y;
        let z = currentCheckpointPos.z - nextCheckpointPos.z;

        let len = Math.sqrt(x * x + y * y + z * z);

        x = x / len;
        y = y / len;
        z = z / len;

        mp.game.graphics.drawMarker(
            checkpointData.transformCheckpoint ? 24 : 0,
            currentCheckpointPos.x, currentCheckpointPos.y, currentCheckpointPos.z + 3,
            x, y, z, // dir
            checkpointData.transformCheckpoint ? 0 : 90, 0, 0, // rot
            3, 3, 3,
            40, 146, 239, 150, // r g b a
            false, false, 2,
            false, "", "", false
        );
    }

    if (checkpointData.secondaryCurrent != null && checkpointData.secondaryNext != null) {
        let x = checkpointData.secondaryCurrent.x - checkpointData.secondaryNext.x;
        let y = checkpointData.secondaryCurrent.y - checkpointData.secondaryNext.y;
        let z = checkpointData.secondaryCurrent.z - checkpointData.secondaryNext.z;

        let len = Math.sqrt(x * x + y * y + z * z);

        x = x / len;
        y = y / len;
        z = z / len;

        mp.game.graphics.drawMarker(0,
            checkpointData.secondaryCurrent.x, checkpointData.secondaryCurrent.y, checkpointData.secondaryCurrent.z + 3,
            x, y, z, // dir
            90, 0, 0, // rot
            3, 3, 3,
            40, 146, 239, 150, // r g b a
            false, false, 2,
            false, "", "", false
        );
    }


    if (checkpointData.secondaryCurrent != null && checkpointData.secondaryNext == null && checkpointData.next != null) {
        let x = checkpointData.secondaryCurrent.x - checkpointData.next.x;
        let y = checkpointData.secondaryCurrent.y - checkpointData.next.y;
        let z = checkpointData.secondaryCurrent.z - checkpointData.next.z;

        let len = Math.sqrt(x * x + y * y + z * z);

        x = x / len;
        y = y / len;
        z = z / len;

        mp.game.graphics.drawMarker(0,
            checkpointData.secondaryCurrent.x, checkpointData.secondaryCurrent.y, checkpointData.secondaryCurrent.z + 3,
            x, y, z, // dir
            90, 0, 0, // rot
            3, 3, 3,
            40, 146, 239, 150, // r g b a
            false, false, 2,
            false, "", "", false
        );
    }
}

mp.events.add('render', () => {
    mp.game.controls.disableControlAction(0, 23, true); //  Enter
    mp.game.controls.disableControlAction(0, 75, true); //  VehicleExit
    
    mp.game.invoke('0x1913FE4CBF41C463', mp.players.local, 32, false); // PED_FLAG_CAN_FLY_THRU_WINDSCREEN 

    mp.game.ui.hideHudComponentThisFrame(6); // vehicle name
    mp.game.ui.hideHudComponentThisFrame(7); // area name
    mp.game.ui.hideHudComponentThisFrame(8); // vehicle class
    mp.game.ui.hideHudComponentThisFrame(9); // street name

    if (mp.game.controls.isDisabledControlPressed(0, 75)) {
        let delta = Date.now() - requestRespawn;
        if (requestRespawn == 0) {
            requestRespawn = Date.now();
        } else if (delta > maxRespawnTime) {
            requestRespawn = 0;
            mp.events.callRemote("RACE_REMOTE_EVENT", "REQUEST_RESPAWN");
            uiBrowser.execute('hideRespawnBar();');
        } else {
            uiBrowser.execute(`setRespawnPercentage(${delta / maxRespawnTime});`);
        }
    } else {
        if (requestRespawn != 0) uiBrowser.execute('hideRespawnBar();');
        requestRespawn = 0;
    }

    if (mp.game.controls.isDisabledControlPressed(0, 58)) { // 58 = ThrowGrenade
        let delta = Date.now() - requestSkip;
        if (requestSkip == 0) {
            requestSkip = Date.now();
        } else if (delta > maxRespawnTime) {
            requestSkip = 0;
            mp.events.callRemote("RACE_REMOTE_EVENT", "VOTESKIP");
            uiBrowser.execute('hideVoteskipBar();');
        } else {
            uiBrowser.execute(`setVoteskipPercentage(${delta / maxRespawnTime});`);
        }
    } else {
        if (requestSkip != 0) uiBrowser.execute('hideVoteskipBar();');
        requestSkip = 0;
    }


    if (checkpointData != null) {
        if (mp.game.controls.isDisabledControlPressed(0, 19) && nitroAmount > 0 && mp.players.local.getVehicleIsUsing() != 0) {
            if (nitroStart == 0) {
                nitroStart = Date.now();
                startNitro();
            } else {
                let delta = Date.now() - nitroStart;

                uiBrowser.execute(`setTurbo(${(nitroAmount - delta) / totalNitro});`);

                if (nitroAmount - delta < 0) {
                    nitroAmount -= delta;
                    nitroStart = 0;
                    stopNitro();
                    uiBrowser.execute(`setTurbo(0);`);
                }

            }
        } else if (nitroStart != 0) {
            let delta = Date.now() - nitroStart;
            nitroAmount -= delta;
            nitroStart = 0;
            stopNitro();

            uiBrowser.execute(`setTurbo(${nitroAmount / totalNitro});`);
        }

        if (activateNitro) {
            let lp = mp.players.local;
            let v = lp.getVehicleIsUsing();
            if (v != null && v != 0) {
                mp.game.invoke('0xB59E4BD37AE292DB', v, 1.5); // Torque Multiplier

                mp.game.invoke('0x4A04DE7CAB2739A1', v, true); // SET_VEHICLE_BOOST_ACTIVE
                mp.game.invoke('0x4A04DE7CAB2739A1', v, false); // SET_VEHICLE_BOOST_ACTIVE

                mp.game.invoke('0x2206BF9A37B7F724', "RaceTurbo", 0, 0); // _START_SCREEN_EFFECT

            }
        } else {

            let lp = mp.players.local;
            let v = lp.getVehicleIsUsing();
            if (v != null && v != 0) {
                mp.game.invoke('0x93A3996368C94158', v, 0); // Power Multiplier
                mp.game.invoke('0xB59E4BD37AE292DB', v, 1.0001); // Torque Multiplier
            }
        }

        if (vehiclesWithNitro.length > 0) {
            vehiclesWithNitro.forEach((v, i, arr) => {
                try {
                    if (mp.game.invoke('0x8702416E512EC454', "core")) {
                        let heading = mp.game.invoke('0xE83D4F9BA2A38914', v.handle); // GET_ENTITY_HEADING
                        let pitch = mp.game.invoke('0xD45DC2893621E1FE', v.handle); // GET_ENTITY_PITCH

                        exhausts.forEach((element, index, arr) => {
                            let boneIndex = mp.game.invoke('0xFB71170B7E76ACBA', v.handle, element); // GET_ENTITY_BONE_INDEX_BY_NAME
                            if (boneIndex >= 0) {
                                let boneCoords = v.getWorldPositionOfBone(boneIndex);


                                mp.game.invoke('0x6C38AF3693A69A91', "core"); // USE_PARTICLE_FX_ASSET
                                //mp.game.graphics.setParticleFxNonLoopedAlpha(0.05);
                                mp.game.graphics.startParticleFxNonLoopedAtCoord("veh_backfire", boneCoords.x, boneCoords.y, boneCoords.z,
                                    0, pitch, heading - 90, 1.01, false, false, false);
                            }
                        });
                    } else {
                        mp.game.invoke('0xB80D8756B4668AB6', "core");
                    }
                } catch (e) {
                    ;
                }
            });
        }

        drawCheckpoints();

        if (lastVehicle != null && lastVehicle != undefined && lastVehicle != 0) {
            let cv = mp.players.local.getVehicleIsUsing();
            if (cv == null || cv == undefined || cv == 0) {
                mp.game.invoke('0xF75B0D629E1C063D', mp.players.local.handle, lastVehicle, -1);
                //mp.players.local.setIntoVehicle(mp.vehicles.atHandle(lastVehicle), -1);
            }
        }

        if (savingVelocity) {        
            let lp = mp.players.local;
            let vh = lp.getVehicleIsUsing();
            if (vh != 0 && vh != null && vh != undefined) {
                let v = mp.vehicles.atHandle(vh);

                if (v != null && v != undefined) {
                    lastVelocity = v.getVelocity();
                }
            }
        }
    }
});

mp.events.add('SET_NEXT_CHECKPOINT', (chdata) => {
    if (!uiBrowser.active)
        uiBrowser.execute('startTimer();');

    if (lastVehicle == null) {
        lastVehicle = mp.players.local.getVehicleIsUsing();
    }

    checkpointData = JSON.parse(chdata);

    checkpointData.current = new mp.Vector3(
        checkpointData.current.x,
        checkpointData.current.y,
        checkpointData.current.z
    );

    if (checkpointData.next != null) {
        checkpointData.next = new mp.Vector3(
            checkpointData.next.x,
            checkpointData.next.y,
            checkpointData.next.z
        );
    }

    if (checkpointData.secondaryCurrent != null) {
        checkpointData.secondaryCurrent = new mp.Vector3(
            checkpointData.secondaryCurrent.x,
            checkpointData.secondaryCurrent.y,
            checkpointData.secondaryCurrent.z
        );
    }

    if (checkpointData.secondaryNext != null) {
        checkpointData.secondaryNext = new mp.Vector3(
            checkpointData.secondaryNext.x,
            checkpointData.secondaryNext.y,
            checkpointData.secondaryNext.z
        );
    }

    if (currentRaceBlip == null) {
        currentRaceBlip = mp.blips.new(1, checkpointData.current, {
            name: "Checkpoint",
            scale: 1,
            color: 66,
            alpha: 255,
            drawDistance: 100,
            shortRange: false,
            rotation: 0,
            dimension: 0,
        });
    } else {
        currentRaceBlip.setAlpha(255);
        currentRaceBlip.setCoords(checkpointData.current);
    }

    if (checkpointData.secondaryCurrent != null) {
        if (currentSecondaryRaceBlip == null) {
            currentSecondaryRaceBlip = mp.blips.new(1, checkpointData.secondaryCurrent, {
                name: "Secondary Checkpoint",
                scale: 1,
                color: 64,
                alpha: 255,
                drawDistance: 100,
                shortRange: false,
                rotation: 0,
                dimension: 0,
            });
        } else {
            currentSecondaryRaceBlip.setAlpha(255);
            currentSecondaryRaceBlip.setCoords(checkpointData.secondaryCurrent);
        }
    } else if (currentSecondaryRaceBlip != null) {
        currentSecondaryRaceBlip.setAlpha(0);
    }

    if (checkpointData.next != null) {
        if (nextRaceBlip == null) {
            nextRaceBlip = mp.blips.new(1, checkpointData.next, {
                name: "Checkpoint",
                scale: 0.7,
                color: 66,
                alpha: 255,
                drawDistance: 100,
                shortRange: false,
                rotation: 0,
                dimension: 0,
            });
        } else {
            nextRaceBlip.setAlpha(255);
            nextRaceBlip.setCoords(checkpointData.next);
        }
    } else if (nextRaceBlip != null) {
        nextRaceBlip.setAlpha(0);
    }

    if (checkpointData.secondaryNext != null) {
        if (nextSecondaryRaceBlip == null) {
            nextSecondaryRaceBlip = mp.blips.new(1, checkpointData.secondaryNext, {
                name: "Secondary Checkpoint",
                scale: 0.7,
                color: 64,
                alpha: 255,
                drawDistance: 100,
                shortRange: false,
                rotation: 0,
                dimension: 0,
            });
        } else {
            nextSecondaryRaceBlip.setAlpha(255);
            nextSecondaryRaceBlip.setCoords(checkpointData.secondaryNext);
        }
    } else if (nextSecondaryRaceBlip != null) {
        nextSecondaryRaceBlip.setAlpha(0);
    }

    if (checkpointData.currentFinish) {
        currentRaceBlip.setSprite(38);
    } else {
        currentRaceBlip.setSprite(1);
        currentRaceBlip.setColour(66);
    }

    if (checkpointData.nextFinish) {
        nextRaceBlip.setSprite(38);
    } else {
        nextRaceBlip.setSprite(1);
        nextRaceBlip.setColour(66);
    }
});

mp.events.add('RESET_CHECKPOINTS', () => {
    checkpointData = null;
    lastVehicle = null;
    if (currentRaceBlip != null) currentRaceBlip.setAlpha(0);
    if (nextRaceBlip != null) nextRaceBlip.setAlpha(0);
});

mp.events.add('PLAY_CHECKPOINT_SOUND', () => {
    playSound("HUD_MINI_GAME_SOUNDSET", "CHECKPOINT_NORMAL");
});

mp.events.add('UPDATE_POSITION', (myPos, ordinalPos, myCheck, totalChecks) => {
    if (!uiBrowser.active) {
        uiBrowser.active = true;
        nitroAmount = totalNitro;
        uiBrowser.execute(`setTurbo(${nitroAmount / totalNitro});`);
    }

    uiBrowser.execute(`SetDataEx(${myCheck}, ${totalChecks}, '${ordinalPos}');`);
});

mp.events.add('CLEAR_POSITION', () => {
    uiBrowser.execute('clearData();');
    uiBrowser.active = false;
});


// SCOREBOARD STATE

let scoreboard = mp.browsers.new("package://proper_race/scoreboard.html");
scoreboard.active = false;

mp.events.add('render', () => {
    // Map voting
    if (mp.game.controls.isDisabledControlJustPressed(0, 201)) // FrontendAccept
    {
        mp.events.callRemote("RACE_REMOTE_EVENT", "MAP_VOTE", true);  // Like
    }

    if (mp.game.controls.isDisabledControlJustPressed(0, 214)) // FrontendDelete
    {
        mp.events.callRemote("RACE_REMOTE_EVENT", "MAP_VOTE", false);  // Like
    }
});

mp.events.add('SHOW_SCOREBOARD', (json) => {
    let data = JSON.parse(json);
    scoreboard.execute('clearRows();');

    scoreboard.execute(`SetDataEx("${data.mapname}", "${data.place}");`)

    data.list.forEach((element, index, array) => {
        scoreboard.execute(`addRow("${element.Position}", "${element.Name}", "${element.Vehicle}", "${element.Time}", "${element.Respawns}", "${element.Rating}");`);
    });

    scoreboard.active = true;
});

mp.events.add('HIDE_SCOREBOARD', () => {
    scoreboard.active = false;    
});

mp.events.add('SET_PLAYER_RATING', (player, rating) => {
    scoreboard.execute(`setPlayerRating("${player}", "${rating}");`);
});

// Camera
mp.events.add('render', () => {
    if (blockCameraInputs) {
        mp.game.controls.disableControlAction(0, 1, true); // LookLeftRight
        mp.game.controls.disableControlAction(0, 2, true); // LookUpDown
        mp.game.controls.disableControlAction(0, 3, true); // LookUpOnly
        mp.game.controls.disableControlAction(0, 4, true); // LookDownOnly
        mp.game.controls.disableControlAction(0, 5, true); // LookLeftOnly
        mp.game.controls.disableControlAction(0, 6, true); // LookRightOnly
        mp.game.controls.disableControlAction(0, 26, true); // LookBehind
        mp.game.controls.disableControlAction(0, 79, true); // VehicleLookBehind
        mp.game.controls.disableControlAction(0, 270, true); // LookLeft
        mp.game.controls.disableControlAction(0, 271, true); // LookRight
        mp.game.controls.disableControlAction(0, 272, true); // LookUp
        mp.game.controls.disableControlAction(0, 273, true); // LookDown
        mp.game.controls.disableControlAction(0, 286, true); // VehicleLookLeft
        mp.game.controls.disableControlAction(0, 287, true); // VehicleLookRight
        mp.game.controls.disableControlAction(0, 290, true); // ScaledLookLeftRight
        mp.game.controls.disableControlAction(0, 291, true); // ScaledLookUpDown
        mp.game.controls.disableControlAction(0, 292, true); // ScaledLookUpOnly
        mp.game.controls.disableControlAction(0, 293, true); // ScaledLookDownOnly
        mp.game.controls.disableControlAction(0, 294, true); // ScaledLookLeftOnly
        mp.game.controls.disableControlAction(0, 295, true); // ScaledLookRightOnly
        mp.game.controls.disableControlAction(0, 329, true); // VehicleDriveLook
        mp.game.controls.disableControlAction(0, 330, true); // VehicleDriveLook2
    }
});

let mainCam = null;
let blockCameraInputs = false;

mp.events.add('CREATE_SKY_CAMERA', (pos) => {
    mp.game.graphics.transitionToBlurred(500);

    mainCam = mp.cameras.new('default', new mp.Vector3(pos.x, pos.y, pos.z + 800.0), new mp.Vector3(0, 0, 0), 40);
    mainCam.pointAtCoord(pos.x, pos.y, pos.z);

    blockCameraInputs = true;
    mp.game.ui.displayRadar(false);

    mainCam.setActive(true);
    mp.game.cam.renderScriptCams(true, false, 0, true, false);
    //mp.game.cam.renderScriptCams(true, true, 5000, true, false);
});

mp.events.add('CLEAR_CAMERA', () => {
    mp.game.graphics.transitionFromBlurred(500);
    if (mainCam != null) {
        try {
            mainCam.setActive(false);
            mainCam.destroy();
        } catch (err) {
            mainCam = null;
        }
        //mp.game.cam.renderScriptCams(false, false, 0, true, false);
        mp.game.cam.renderScriptCams(false, true, 5000, true, false);

        mp.game.cam.setGameplayCamRelativePitch(0.0, 1.0);
        mp.game.cam.setGameplayCamRelativeHeading(0.0);

        setTimeout(() => {
            mp.game.ui.displayRadar(true);
            blockCameraInputs = false;
        }, 5000);
    }
});

// VOTEMAP STATE

let votemapBoard = mp.browsers.new("package://proper_race/voting.html");
votemapBoard.active = false;

mp.events.add('render', () => {
    if (votemapBoard.active) {
        if (mp.game.controls.isDisabledControlJustPressed(0, 187)) { // FrontendDown    
            playSound("HUD_FRONTEND_DEFAULT_SOUNDSET", "NAV_UP_DOWN");
            votemapBoard.execute("goDown();");
        }
        if (mp.game.controls.isDisabledControlJustPressed(0, 188)) { // FrontendUp
            playSound("HUD_FRONTEND_DEFAULT_SOUNDSET", "NAV_UP_DOWN");
            votemapBoard.execute("goUp();");
        }
        if (mp.game.controls.isDisabledControlJustPressed(0, 189)) { // FrontendLeft        
            playSound("HUD_FRONTEND_DEFAULT_SOUNDSET", "NAV_LEFT_RIGHT");
            votemapBoard.execute("goLeft();");
        }
        if (mp.game.controls.isDisabledControlJustPressed(0, 190)) { // FrontendRight        
            playSound("HUD_FRONTEND_DEFAULT_SOUNDSET", "NAV_LEFT_RIGHT");
            votemapBoard.execute("goRight();");
        }

        if (mp.game.controls.isDisabledControlJustPressed(0, 201)) { // FrontendAccept        
            playSound("HUD_FRONTEND_DEFAULT_SOUNDSET", "SELECT");
            votemapBoard.execute("submit();");
        }
    }
});

mp.events.add('CREATE_VOTE_BOARD', (json) => {
    votemapBoard.execute(`SetDataEx(${json});`);

    votemapBoard.active = true;
    mp.gui.chat.show(false);
});

mp.events.add('UPDATE_VOTES', (votes, done, total) => {
    votemapBoard.execute(`setVotes(${votes});`);
    votemapBoard.execute(`updateVoteNumber(${done}, ${total});`);
});

mp.events.add('DESTROY_VOTE_BOARD', () => {
    votemapBoard.active = false;
    mp.gui.chat.show(true);
});

mp.events.add('SUBMIT_MAP_VOTE', (vote) => {
    mp.events.callRemote("RACE_REMOTE_EVENT", "SUBMIT_MAP_VOTE", vote);
});

/* SCALEFORM STUFF */

function playSound(dict, name) {
    mp.game.audio.playSoundFrontend(1, name, dict, true);
}

function callFunctionHead(scaleform, name) {
    mp.game.graphics.pushScaleformMovieFunction(scaleform, name);
}

function pushString(str) {
    mp.game.graphics.pushScaleformMovieFunctionParameterString(str)
}

function pushInt(val) {
    mp.game.graphics.pushScaleformMovieFunctionParameterInt(val);
}

function pushBool(val) {
    mp.game.graphics.pushScaleformMovieFunctionParameterBool(val);
}

function pushFloat(val) {
    mp.game.graphics.pushScaleformMovieFunctionParameterFloat(val);
}

function callFunctionEnd() {
    //NATIVES.GRAPHICS._POP_SCALEFORM_MOVIE_FUNCTION();
    mp.game.graphics.popScaleformMovieFunctionVoid();
}

function getPositionFromFace(pos, angle, distance) {
    let x = pos.x + Math.cos((Math.PI / 180) * angle) * distance;
    let y = pos.y + Math.sin((Math.PI / 180) * angle) * distance;

    return new mp.Vector3(x, y, pos.z);
}