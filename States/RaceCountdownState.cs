﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProperCuntingRacingResource.States
{
    public class RaceCountdownState : GameState
    {
        public override string StateName => nameof(RaceCountdownState);

        private readonly Race _race;
        private readonly Client[] _readyPlayers;

        private readonly List<Client> _pendingSpawns;

        private DateTime _countdownStart;

        public RaceCountdownState(Race race, Client[] readyPlayers)
        {
            _race = race;
            _readyPlayers = readyPlayers;

            _pendingSpawns = new List<Client>();
        }

        public override void EnterState(GameState previous)
        {
            _countdownStart = DateTime.UtcNow;
            NAPI.Chat.SendChatMessageToAll("The ~r~race~w~ is starting! Ready your engines!");

            foreach (var player in _readyPlayers)
            {
                player.SetDataEx("CHECKPOINTS_PASSED", 0);
                player.SetDataEx("WON_RACE", false);
                player.SetDataEx("IN_RACE", true);

                player.TriggerEvent("RACE_START_COUNTDOWN", _race.Name);

                Vehicle playersCar = (Vehicle)player.GetDataEx("RACE_VEHICLE");                
                foreach (var p in NAPI.Pools.GetAllPlayers()) p.TriggerEvent("SET_VEHICLE_GHOSTMODE", playersCar, true);
            }
        }

        public override void OnUpdate()
        {
            if ((DateTime.UtcNow - _countdownStart).TotalSeconds >= 10)
            {
                // Start race
                NAPI.Chat.SendChatMessageToAll("~g~Race has started!");

                foreach (var client in _readyPlayers)
                {
                    // Unfreeze everyone.
                    Vehicle vehicle = (Vehicle)client.GetDataEx("RACE_VEHICLE");                    
                    Util.FreezeEntityForPlayer(client, vehicle, false);

                    int @class = NAPI.Vehicle.GetVehicleClass((VehicleHash) vehicle.Model);
                    
                    if (@class == 16 || @class == 15) // planes & helis
                    {
                        client.TriggerEvent("SET_BLADES_FULLSPEED", vehicle);
                    }
                }

                // Transition to race state
                Console.WriteLine("Race has started!");
                Master.TransitionState(new OngoingRaceState(_race, _readyPlayers.ToList(), _pendingSpawns));
            }
        }

        public override void OnPlayerDeath(Client player, Client killer, uint reason)
        {
            NAPI.Player.SpawnPlayer(player, _race.Trigger);

            if (player.HasDataEx("RACE_VEHICLE") && player.GetDataEx("RACE_VEHICLE") != null)
            {
                Vehicle car = (Vehicle)player.GetDataEx("RACE_VEHICLE");
                car.Delete();
                player.ResetDataEx("RACE_VEHICLE");
            }

            _pendingSpawns.Add(player);
            player.SendChatMessage("~r~[IMPORTANT]~w~ The race has just begun, we will place you in 10 seconds.");
        }

        public override void OnPlayerConnected(Client player)
        {
            player.SendChatMessage("~r~[IMPORTANT]~w~ The race has just begun, we will place you in 10 seconds.");

            player.SetDataEx("CHECKPOINTS_PASSED", 0);
            player.SetDataEx("WON_RACE", false);
            player.SetDataEx("IN_RACE", false);

            _pendingSpawns.Add(player);
        }

        public override void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            if (player.HasDataEx("RACE_VEHICLE") && player.GetDataEx("RACE_VEHICLE") != null)
            {
                Vehicle car = (Vehicle)player.GetDataEx("RACE_VEHICLE");
                car.Delete();
            }

            if (NAPI.Pools.GetAllPlayers().Count == 1)
            {
                Master.TransitionState(new RaceCleanupState());
            }
        }
    }
}
