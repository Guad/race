﻿using GTANetworkAPI;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace ProperCuntingRacingResource.States
{
    public class RaceLoadingState : GameState
    {
        // In this state we load all of the objects, telepor players, load their vehicles, put them inside and freeze them.
        // Then everyone should wait for everybody.
        // Other players can join the race at this time.
        public override string StateName => nameof(RaceLoadingState);
        
        private readonly Race _race;
        private int _racePosition;
        private Entity _firstEntity;

        private List<Client> _haveLoadedBag;
        private Queue<Client> _racePlacementQueue;
        private List<Client> _loadingObjects;

        private DateTime _loadingStartTime;
        private const int MAX_WAITING_SECONDS = 60;

        private RacerPlacer _placer;

        public RaceLoadingState(Race race)
        {
            _race = race;
            _racePlacementQueue = new Queue<Client>();

            _haveLoadedBag = new List<Client>();
            _loadingObjects = new List<Client>();

            _placer = new RacerPlacer(true);
            _placer.PlayerLoaded = (p) =>
            {
                Console.WriteLine("Player {0} has loaded!", p.Name);
                if (!_haveLoadedBag.Contains(p))
                    _haveLoadedBag.Add(p);
            };
        }

        public override void EnterState(GameState previous)
        {
            NAPI.Chat.SendChatMessageToAll("Now traveling to ~b~" + _race.Name + "~w~...");

            _loadingStartTime = DateTime.Now;

            NAPI.World.SetTime(Util.GetRandInt(24), 0, 0);

            double weatherChance = Util.GetRand();

            for (int i = 0; i < 14; i++)
            {
                if (weatherChance > (1d / i))
                {
                    NAPI.World.SetWeather((Weather)i);
                    break;
                }
            }

            foreach (var p in NAPI.Pools.GetAllPlayers())
            {
                PreparePlayer(p);

                if (_race.DecorativeProps.Length > 0)
                    p.Position = _race.DecorativeProps[0].Position;
            }

            foreach (var decorativeProp in _race.DecorativeProps)
            {
                NAPI.Object.CreateObject(decorativeProp.Hash, decorativeProp.Position, decorativeProp.Rotation);
            }

            for (int i = 0; i < _race.Checkpoints.Length; i++)
            {
                float sizeScale = 1f;

                if (_race.CheckpointSizes != null && _race.CheckpointSizes.Length > 0)
                {
                    sizeScale = _race.CheckpointSizes[i];
                }

                int? tv = null;
                if (_race.TransformCheckpoints != null &&
                    _race.TransformCheckpoints.Length > i &&
                    _race.TransformCheckpoints[i] != -1)
                {
                    tv = _race.TransformModels[_race.TransformCheckpoints[i]];
                }

                Checkpoints.Add(new Checkpoints.Checkpoint()
                {
                    Position = _race.Checkpoints[i],
                    Size = 16f * sizeScale,
                    Id = i,
                    TransformVehicle = tv,
                });
            }

            if (_race.SecondaryCheckpoints != null)
            for (int i = 0; i < _race.SecondaryCheckpoints.Length; i++)
            {
                    float sizeScale = 1f;

                    if (_race.CheckpointSizes != null && _race.CheckpointSizes.Length > 0)
                    {
                        sizeScale = _race.CheckpointSizes[i];
                    }

                    int? tv = null;
                    if (_race.TransformCheckpoints != null &&
                        _race.TransformCheckpoints.Length > i &&
                        _race.TransformCheckpoints[i] != -1)
                    {
                        tv = _race.TransformModels[_race.TransformCheckpoints[i]];
                    }

                    Checkpoints.Add(new Checkpoints.Checkpoint()
                    {
                        Position = _race.SecondaryCheckpoints[i],
                        Size = 16f * sizeScale,
                        Id = i,
                        TransformVehicle = tv,
                        Secondary = true,
                    });
                }

            if (_race.DecorativeProps.Length > 0)
            {
                _firstEntity = NAPI.Object.CreateObject(
                    NAPI.Util.GetHashKey("apa_mp_apa_yacht_jacuzzi_ripple1"),
                    _race.SpawnPoints[0].Position,
                    new Vector3()
                    );

                foreach (var client in NAPI.Pools.GetAllPlayers())
                {
                    client.TriggerEvent("CHECK_OBJECT_PLACEMENT", _firstEntity);
                    _loadingObjects.Add(client);
                }
            }
            else
            {
                foreach (var client in NAPI.Pools.GetAllPlayers())
                {
                    _racePlacementQueue.Enqueue(client);
                }
            }
        }

        public override void LeaveState(GameState next)
        {
            foreach (var player in NAPI.Pools.GetAllPlayers())
            {
                player.TriggerEvent("HIDE_INSTRUCTIONAL_BUTTONS");
                player.TriggerEvent("CLEAR_CAMERA");
                player.ResetDataEx("HAS_BEEN_PLACED");
            }
        }

        public override void OnPlayerConnected(Client player)
        {            
            if (_race.DecorativeProps.Length > 0)
            {
                player.TriggerEvent("CHECK_OBJECT_PLACEMENT", _firstEntity);
                _loadingObjects.Add(player);
            }
            else
            {
                _racePlacementQueue.Enqueue(player);
            }

            PreparePlayer(player);
        }

        public override void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            _placer.RemovePlayer(player);
            _haveLoadedBag.Remove(player);
            _loadingObjects.Remove(player);
            
            if (player.HasDataEx("RACE_VEHICLE") && player.GetDataEx("RACE_VEHICLE") != null)
            {
                Vehicle car = (Vehicle)player.GetDataEx("RACE_VEHICLE");
                car.Delete();
                player.ResetDataEx("RACE_VEHICLE");
            }

            if (NAPI.Pools.GetAllPlayers().Count == 1)
            {
                Master.TransitionState(new RaceCleanupState());
            }
        }

        public override void OnUpdate()
        {
            if (_racePlacementQueue.Count > 0)
            {
                Client client = _racePlacementQueue.Dequeue();

                SpawnPoint sp = _race.SpawnPoints[_racePosition++ % _race.SpawnPoints.Length];
                PlacePlayer(client, sp, _race.AvailableVehicles[Util.GetRandInt(_race.AvailableVehicles.Length)]);
            }
            /*else if ((DateTime.Now - _loadingStartTime).TotalSeconds >= MAX_WAITING_SECONDS)
            {
                // Kick rest of the players
                foreach (var client in _stillLoadingBag)
                {
                    client.Kick("Too slow loading");
                }

                _stillLoadingBag.Clear();
                StartRace();
            }*/
            else if (
                _placer.PlayersLoading == 0 &&
                _racePlacementQueue.Count == 0 &&
                _loadingObjects.Count == 0)
            {
                StartRace();
            }

            _placer.Update();
        }

        

        public override void OnPlayerDeath(Client player, Client killer, uint reason)
        {
            NAPI.Player.SpawnPlayer(player, _race.Trigger);

            if (_haveLoadedBag.Contains(player))
            {
                _haveLoadedBag.Remove(player);
            }

            _racePlacementQueue.Enqueue(player);

        }

        public override void OnRemoteEvent(Client player, string name, object[] args)
        {
            if (name == "OBJECT_PLACEMENT_OK")
            {
                _racePlacementQueue.Enqueue(player);
                _loadingObjects.Remove(player);

                Console.WriteLine("Player {0} loaded all objects & started placing veh", player.Name);
            }
            else if (name == "RESEND_OBJECT_PLACEMENT")
            {
                player.TriggerEvent("CHECK_OBJECT_PLACEMENT", _firstEntity);
            }

            _placer.RemoteEvent(player, name, args);
        }

        // Private methods

        private void PreparePlayer(Client p)
        {
            p.ResetDataEx("HAS_LOADED");
            //p.ResetDataEx("RACE_VEHICLE");

            p.TriggerEvent("SET_LOADING_TEXT", "Loading " + _race.Name);
            p.TriggerEvent("CREATE_SKY_CAMERA", _race.Trigger);

            if (_race.TransformModels != null && _race.TransformModels.Length > 0)
            {
                string json = Newtonsoft.Json.JsonConvert.SerializeObject(_race.TransformModels.Where(m => m != 0).ToArray());
                // TODO: serialize json only once

                p.TriggerEvent("REQUEST_VEHICLES", json);
            }
        }

        
        private void StartRace()
        {
            if (_firstEntity != null)
                _firstEntity.Delete();

            Master.TransitionState(new RaceCountdownState(_race, _haveLoadedBag.ToArray()));
        }

        
        private void PlacePlayer(Client player, SpawnPoint sp, VehicleHash carmodel)
        {
            player.SetDataEx("ORIGINAL_VEHICLE_MODEL", (int)carmodel);
            _placer.EnqueuePlayer(player, sp, (int)carmodel);
        }
    }
}
