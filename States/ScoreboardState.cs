﻿using GTANetworkAPI;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace ProperCuntingRacingResource.States
{
    public class ScoreboardState : GameState
    {
        public override string StateName => nameof(ScoreboardState);

        private int _likes = 0;
        private int _dislikes = 0;

        private readonly Race _race;
        private readonly Vector3 _finish;
        private readonly DateTime _startTime;
        private const int SCOREBOARD_TIME_SECONDS = 20;
        private List<ScoreboardData> _scoreboardPlayers;

        class ScoreboardData
        {
            public string Position;
            public string Name;
            public string Vehicle;
            public string Time;
            public string Respawns;
            public string Rating;
        }

        public ScoreboardState(Race race, Vector3 finish)
        {
            this._race = race;
            this._finish = finish;
            _startTime = DateTime.UtcNow;
        }

        public override void EnterState(GameState previous)
        {
            _scoreboardPlayers = new List<ScoreboardData>();

            foreach(var player in NAPI.Pools.GetAllPlayers()
                .Where(p => p.HasDataEx("RACE_POSITION") && ((int)p.GetDataEx("RACE_POSITION")) > 0)
                .OrderBy(p => (int) p.GetDataEx("RACE_POSITION")))
            {
                int position = (int) player.GetDataEx("RACE_POSITION");
                string name = player.Name;
                int respawns = 0;
                if (player.HasDataEx("RESPAWNS_USED"))
                    respawns = (int) player.GetDataEx("RESPAWNS_USED");
                int model = (int)player.GetDataEx("VEHICLE_MODEL");
                bool hasRating = player.HasDataEx("LIKED_RACE");
                bool liked = false;
                if (hasRating)
                    liked = (bool)player.GetDataEx("LIKED_RACE");

                int time = -1;
                if (player.HasDataEx("RACE_TIME")) time = (int) (double)player.GetDataEx("RACE_TIME");

                ScoreboardData data = new ScoreboardData()
                {
                    Position = position.ToString(),
                    Name = name.Replace("\"", "\\\"").Replace("'", "\\'"),
                    Vehicle = NAPI.Vehicle.GetVehicleDisplayName((VehicleHash) model) ?? "?",
                    Time = time == -1 ? "DNF" : TimeSpan.FromMilliseconds(time).ToString("mm':'ss'.'fff"),
                    Respawns = respawns.ToString(),
                    Rating = hasRating ? liked ? "LIKE" : "DISLIKE" : "",
                };

                _scoreboardPlayers.Add(data);
            }


            foreach (var player in NAPI.Pools.GetAllPlayers())
            {
                player.TriggerEvent("CREATE_SKY_CAMERA", _finish);

                player.ResetDataEx("LIKED_RACE");

                string position = "DNF";

                if (player.HasDataEx("RACE_POSITION"))
                    position = Util.AddOrdinal((int)player.GetDataEx("RACE_POSITION")) + " Place";

                string json = Newtonsoft.Json.JsonConvert.SerializeObject(
                    new {
                        list = _scoreboardPlayers,
                        mapname = _race.Name,
                        place = position
                    });
            
                player.TriggerEvent("SHOW_SCOREBOARD", json);

                var instructionalButtonsJson = Newtonsoft.Json.JsonConvert.SerializeObject(
                        new object[]
                        {
                            new object[] { 201, "Like" },
                            new object[] { 214, "Dislike" }// 202, 214 = FrntendDelete
                        }
                    );

                player.TriggerEvent("SET_INSTRUCTIONAL_BUTTONS", instructionalButtonsJson);
            }
        }

        public override void LeaveState(GameState next)
        {
            foreach (var player in NAPI.Pools.GetAllPlayers())
            {
                player.ResetDataEx("LIKED_RACE");
                player.TriggerEvent("HIDE_SCOREBOARD");    
                player.TriggerEvent("HIDE_INSTRUCTIONAL_BUTTONS");
            }
        }

        public override void OnUpdate()
        {
            if ((DateTime.UtcNow - _startTime).TotalSeconds >= SCOREBOARD_TIME_SECONDS)
            {
                Master.DatabaseContext.UpdateMapRating(_race.Filename, _likes, _dislikes);

                Master.TransitionState(new VotemapState(_race));
            }
        }

        public override void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            if (NAPI.Pools.GetAllPlayers().Count == 1)
            {
                Master.TransitionState(new RaceCleanupState());
            }
        }

        public override void OnRemoteEvent(Client player, string name, object[] args)
        {
            if (name == "MAP_VOTE")
            {
                bool like = (bool)args[0];

                if (player.HasDataEx("RACE_POSITION") && !player.HasDataEx("LIKED_RACE"))
                {
                    // TODO: Save race likes

                    if (like) _likes++;
                    else _dislikes++;

                    player.SetDataEx("LIKED_RACE", like);
                    player.TriggerEvent("HIDE_INSTRUCTIONAL_BUTTONS");

                    foreach (var p in NAPI.Pools.GetAllPlayers())
                    {
                        p.TriggerEvent("SET_PLAYER_RATING", player.Name, like ? "LIKE" : "DISLIKE");
                    }
                }
            }
        }

        public override void OnPlayerConnected(Client player)
        {
            player.TriggerEvent("CREATE_SKY_CAMERA", _finish);

            player.ResetDataEx("LIKED_RACE");

            string position = "DNF";

            if (player.HasDataEx("RACE_POSITION"))
                position = Util.AddOrdinal((int)player.GetDataEx("RACE_POSITION")) + " Place";

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(
                new
                {
                    list = _scoreboardPlayers,
                    mapname = _race.Name,
                    place = position
                });

            player.TriggerEvent("SHOW_SCOREBOARD", json);
        }
    }
}
