﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProperCuntingRacingResource.States
{
    public class PreGameState : GameState
    {
        public override string StateName => nameof(PreGameState);

        private int _playerCount;
        private const int MIN_PLAYERS_FOR_RACE = 1;

        public override void EnterState(GameState previous)
        {
            _playerCount = NAPI.Pools.GetAllPlayers().Count;
        }

        public override void LeaveState(GameState next)
        {
            
        }

        public override void OnPlayerConnected(Client player)
        {
            _playerCount++;

            TryStartRace();
        }

        public override void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            _playerCount--;
        }

        public override void OnUpdate()
        {
            if (_playerCount >= MIN_PLAYERS_FOR_RACE) TryStartRace();
        }

        private void TryStartRace()
        {
            if (_playerCount >= MIN_PLAYERS_FOR_RACE)
            {
                // Start random race
                RaceParser.AvailableMap randomMap = RaceParser.AllMaps[Util.GetRandInt(RaceParser.AllMaps.Length)];
                //NAPI.Chat.SendChatMessageToAll($"Starting race ~r~{randomMap.Name}~w~!");
                Race randomRace = RaceParser.Parse(randomMap.Path);

                Master.TransitionState(new RaceLoadingState(randomRace));
            }
        }
    }
}
