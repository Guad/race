﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProperCuntingRacingResource.States
{
    class RaceCleanupState : GameState
    {
        public override string StateName => nameof(RaceCleanupState);

        private Race _nextRace;
        public RaceCleanupState(Race next = null)
        {
            _nextRace = next;
        }

        public override void EnterState(GameState previous)
        {
            List<Vehicle> vehicles = NAPI.Pools.GetAllVehicles();
            List<GTANetworkAPI.Object> props = NAPI.Pools.GetAllObjects();
            List<Client> players = NAPI.Pools.GetAllPlayers();

            foreach (var item in vehicles)
                item.Delete();

            foreach (var item in props)
                item.Delete();

            foreach (var item in players)
            {
                foreach (var data in NAPI.Data.GetAllEntityData(item))
                {
                    item.ResetDataEx(data);
                }
            }

            Checkpoints.Clean();
            DataExtension.Clean();
        }

        public override void OnUpdate()
        {
            if (_nextRace != null)
            {
                Master.TransitionState(new RaceLoadingState(_nextRace));
            }
            else
            {
                Master.TransitionState(new PreGameState());
            }
        }
    }
}
