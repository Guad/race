﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProperCuntingRacingResource.States
{
    public class VotemapState : GameState
    {
        class PotentialMap
        {
            public int Index;
            public string Name;
            public string Description;
            public string ImageB64;
            public int Checkpoints;
            public int Spawnpoints;
            public int Props;            
            public string Thumbnail;
            public int Likes;
            public int Dislikes;
            public string Rating;
        }

        public override string StateName => nameof(VotemapState);

        private readonly Race _lastRace;
        private readonly int _refreshesLeft;
        private List<Race> _maps;
        private List<PotentialMap> _candidates;
        private string _boardJson;
        private int _peopleVoted;
        private DateTime _voteStart;
        private bool _clearCam = true;

        private const int VOTE_TIMEOUT_SECONDS = 30;

        public VotemapState(Race lastRace, int refreshesLeft = 1)
        {
            this._lastRace = lastRace;
            _refreshesLeft = refreshesLeft;

            _maps = new List<Race>();
            _candidates = new List<PotentialMap>();
        }

        public override void EnterState(GameState previous)
        {
            int max = RaceParser.AllMaps.Length;

            _voteStart = DateTime.UtcNow;

            List<int> chosen = new List<int>();

            while (chosen.Count < 6)
            {
                int n = Util.GetRandInt(max);

                if (!chosen.Contains(n))
                    chosen.Add(n);
            }

            foreach (var it in chosen)
                _maps.Add(RaceParser.Parse(RaceParser.AllMaps[it].Path));

            int i = 0;
            foreach (var map in _maps)
            {
                int likes, dislikes;

                Master.DatabaseContext.GetMapRating(map.Filename, out likes, out dislikes);

                _candidates.Add(new PotentialMap()
                {
                    Index = i++,
                    Name = map.Name,
                    Description = map.Description,
                    ImageB64 = "",
                    Checkpoints = map.Checkpoints.Length,
                    Spawnpoints = map.SpawnPoints.Length,
                    Props = map.DecorativeProps.Length,
                    Thumbnail = map.Thumbnail,
                    Likes = likes,
                    Dislikes = dislikes,
                    Rating = likes + dislikes == 0 ? "??" : ((float) likes / (likes + dislikes)).ToString("P2"),
                });
            }

            int totalVotes = NAPI.Pools.GetAllPlayers().Count;

            _boardJson = Newtonsoft.Json.JsonConvert.SerializeObject(
                new
                {
                    maps = _candidates,
                    refreshesLeft = _refreshesLeft,
                    totalVotes = totalVotes,
                });

            foreach (var p in NAPI.Pools.GetAllPlayers())
            {
                p.TriggerEvent("CREATE_VOTE_BOARD", _boardJson);
            }
        }

        public override void LeaveState(GameState next)
        {
            foreach (var p in NAPI.Pools.GetAllPlayers())
            {
                p.ResetDataEx("MAP_VOTE");
                p.TriggerEvent("DESTROY_VOTE_BOARD");
                if (_clearCam) p.TriggerEvent("CLEAR_CAMERA");
            }
        }

        public override void OnRemoteEvent(Client player, string name, object[] args)
        {
            if (name == "SUBMIT_MAP_VOTE")
            {
                player.SetDataEx("MAP_VOTE", (int)args[0]);
                ResendCurrentVotes();
            }
        }

        public override void OnPlayerConnected(Client player)
        {
            player.TriggerEvent("CREATE_VOTE_BOARD", _boardJson);

            ResendCurrentVotes();
        }

        public override void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            ResendCurrentVotes();

            if (NAPI.Pools.GetAllPlayers().Count == 1)
            {
                Master.TransitionState(new RaceCleanupState());
            }
        }

        public override void OnUpdate()
        {
            if ((DateTime.UtcNow - _voteStart).TotalSeconds >= VOTE_TIMEOUT_SECONDS || NAPI.Pools.GetAllPlayers().Count == _peopleVoted)
            {
                // Decide who won
                int[] votes = new int[9];
                var playas = NAPI.Pools.GetAllPlayers();

                for (int i = 0; i < playas.Count; i++)
                {
                    if (playas[i].HasDataEx("MAP_VOTE"))
                    {
                        int vote = (int)playas[i].GetDataEx("MAP_VOTE");
                        votes[vote]++;
                    }
                }

                int maxVote = votes[0];
                int maxVoteIndex = 0;

                for (int i = 1; i < votes.Length; i++)
                {
                    if (votes[i] > maxVote)
                    {
                        maxVote = votes[i];
                        maxVoteIndex = i;
                    }
                }

                if (maxVoteIndex < 6) // Map won
                {
                    Master.TransitionState(new RaceCleanupState(_maps[maxVoteIndex]));
                }
                else
                {
                    switch (maxVoteIndex)
                    {
                        case 6: // Replay
                            Master.TransitionState(new RaceCleanupState(_lastRace));
                            break;
                        case 7: // Refresh
                            _clearCam = false;
                            Master.TransitionState(new VotemapState(_lastRace, _refreshesLeft - 1));
                            break;
                        case 8: // Random
                            Master.TransitionState(new RaceCleanupState());
                            break;
                    }
                }

            }
        }

        private void ResendCurrentVotes()
        {
            var playas = NAPI.Pools.GetAllPlayers();

            int playerVotes = 0;

            int[] votes = new int[9];

            for (int i = 0; i < playas.Count; i++)
            {
                if (playas[i].HasDataEx("MAP_VOTE"))
                {
                    int vote = (int)playas[i].GetDataEx("MAP_VOTE");
                    playerVotes++;
                    votes[vote]++;                    
                }
            }

            _peopleVoted = playerVotes;

            string votesJson = Newtonsoft.Json.JsonConvert.SerializeObject(votes);

            foreach (var p in playas)
            {
                p.TriggerEvent("UPDATE_VOTES", votesJson, playerVotes, playas.Count);
            }
        }
    }
}
