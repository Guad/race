﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ProperCuntingRacingResource.States
{
    public class OngoingRaceState : GameState
    {
        private readonly Race _race;
        private readonly List<Client> _pendingSpawns;
        private readonly List<Client> _playersInRace;

        private readonly List<Client> _finishedPlayers;
        private readonly Queue<Client> _playerRespawnQueue;

        private DateTime _raceStart;
        private DateTime _lastPositionUpdate;
        private DateTime? _firstPlayerFinished;

        private Util.Delayer _delayer = new Util.Delayer();
        private RacerPlacer _placer;

        private const int NO_SPAWN_PERIOD_SECONDS = 10;
        private const int POSITION_UPDATE_INTERVAL_MS = 2000;
        private const int TIMEOUT_AFTER_FIRST_PLAYER_FINISHES_SECONDS = 60;

        private int _votesToSkip = 0;

        public override string StateName => nameof(OngoingRaceState);

        public OngoingRaceState(Race race, List<Client> readyPlayers, List<Client> pendingSpawns)
        {
            _race = race;
            _pendingSpawns = pendingSpawns;
            _playersInRace = readyPlayers;

            _finishedPlayers = new List<Client>();
            _playerRespawnQueue = new Queue<Client>();
            _lastPositionUpdate = DateTime.UtcNow;

            _placer = new RacerPlacer(false);
            _placer.PlayerFailedToLoad = (player) =>
            {
                if (player == null) return;

                player.Kick("You did not load in time.");
                /*
                _placer.EnqueuePlayer(player, new SpawnPoint()
                {
                    Position = (Vector3) player.GetDataEx("SPAWNPOINT"),
                    Heading = (float)player.GetDataEx("HEADING"),
                },
                (int) player.GetDataEx("VEHICLE_MODEL"));
                */
            };

            _placer.PlayerLoaded = (player) =>
            {
                if (!_playersInRace.Contains(player))
                    _playersInRace.Add(player);

                Vehicle playersCar = (Vehicle) player.GetDataEx("RACE_VEHICLE");

                _delayer.DelayAction(500, () =>
                {
                    foreach (var p in NAPI.Pools.GetAllPlayers()) if (p != player) p.TriggerEvent("SET_VEHICLE_GHOSTMODE", playersCar, true);
                    player.TriggerEvent("UPDATE_VEHICLE_GHOSTMODES");
                });

                _delayer.DelayAction(3000, () =>
                {
                    //NAPI.ClientEvent.TriggerClientEventForAll("SET_VEHICLE_GHOSTMODE", playersCar, false);                    
                    foreach (var p in NAPI.Pools.GetAllPlayers()) if (p != player) p.TriggerEvent("SET_VEHICLE_GHOSTMODE", playersCar, false);
                });


                int @class = NAPI.Vehicle.GetVehicleClass((VehicleHash) playersCar.Model);

                if (@class == 16 || @class == 15) // planes & helis
                {
                    player.TriggerEvent("SET_BLADES_FULLSPEED", playersCar);
                }

                player.TriggerEvent("FADE_SCREEN", true);
            };

            Checkpoints.OnPlayerInsideCheckpoint = PlayerInsideCheckpoint;
        }

        public override void EnterState(GameState previous)
        {
            _raceStart = DateTime.UtcNow;

            var instructionalButtonsJson = Newtonsoft.Json.JsonConvert.SerializeObject(
                new object[]
                {
                    new object[] {58, "Voteskip" },
                    new object[] {75, "Respawn" },
                    new object[] {19, "Turbo"},
                }
                );

            foreach (var player in _playersInRace)
            {                
                player.ResetDataEx("RACE_TIME");                

                player.SetDataEx("RACE_POSITION", 1);
                player.SetDataEx("RESPAWNS_USED", 0);

                player.TriggerEvent("SET_INSTRUCTIONAL_BUTTONS", instructionalButtonsJson);

                SendNextCheckpoint(player, 0);                
            }
        }

        public override void LeaveState(GameState next)
        {
            foreach (var player in NAPI.Pools.GetAllPlayers())
            {
                NAPI.Player.SpawnPlayer(player, _race.Trigger);

                player.TriggerEvent("CLEAR_POSITION");
                player.TriggerEvent("HIDE_INSTRUCTIONAL_BUTTONS");
                player.TriggerEvent("RESET_CHECKPOINTS");

                player.TriggerEvent("CLEAR_REQUESTED_VEHICLES");

                player.ResetDataEx("HAS_VOTED");
                player.ResetDataEx("IN_RACE");
                //player.ResetDataEx("RACE_VEHICLE");
                //player.ResetDataEx("RACE_POSITION");
                player.ResetDataEx("CHECKPOINTS_PASSED");
            }
        }

        public override void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            if (player.HasDataEx("RACE_VEHICLE") && player.GetDataEx("RACE_VEHICLE") != null)
            {
                Vehicle car = (Vehicle)player.GetDataEx("RACE_VEHICLE");
                car.Delete();
            }

            _playersInRace.Remove(player);
            _pendingSpawns.Remove(player);

            if (NAPI.Pools.GetAllPlayers().Count == 1)
            {
                Master.TransitionState(new RaceCleanupState());
            }
        }

        public override void OnPlayerConnected(Client player)
        {
            player.SetDataEx("RACE_POSITION", -1);
            player.SetDataEx("IN_RACE", false);
            player.SetDataEx("HAS_LOADED", false);
            player.SetDataEx("CHECKPOINTS_PASSED", 0);
            player.SetDataEx("RESPAWNS_USED", 0);

            SendNextCheckpoint(player, 0);

            _pendingSpawns.Add(player);
        }

        public override void OnPlayerDeath(Client player, Client killer, uint reason)
        {
            /*_delayer.DelayAction(3000, () =>
            {
                NAPI.Player.SpawnPlayer(player, _race.Trigger);
            });
            _delays.Add(new Delay()
            {
                When = DateTime.UtcNow.Add(TimeSpan.FromSeconds(5)),
                Callback = () =>
                {
                    if (!_finishedPlayers.Contains(player))
                        _pendingSpawns.Add(player);
                },
            });*/
        }

        public override void OnRemoteEvent(Client player, string name, object[] args)
        {
            _placer.RemoteEvent(player, name, args);

            if (name == "REQUEST_RESPAWN" && (bool) player.GetDataEx("IN_RACE") == true)
            {
                if (!player.HasDataEx("RESPAWNS_USED")) player.SetDataEx("RESPAWNS_USED", 0);
                player.TriggerEvent("FADE_SCREEN", false);
                if (!_pendingSpawns.Contains(player)) _pendingSpawns.Add(player);
                player.SetDataEx("RESPAWNS_USED", ((int) player.GetDataEx("RESPAWNS_USED")) + 1);
                player.TriggerEvent("GIVE_NITRO", 2500);
            }
            else if (name == "VOTESKIP")
            {
                if (player.HasDataEx("HAS_VOTED") && (bool)player.GetDataEx("HAS_VOTED") == true) return;
                long now = DateTimeOffset.Now.ToUnixTimeSeconds();

                if (NAPI.Data.HasWorldData("LAST_VOTESKIP_SUCCESS"))
                {
                    long lastTime = NAPI.Data.GetWorldData("LAST_VOTESKIP_SUCCESS");
                    if (now - lastTime < 3 * 60) // 3 minutes
                    {
                        player.SendChatMessage("Please wait some time before voting to skip again.");
                        return;
                    }
                }

                player.SetDataEx("HAS_VOTED", true);
                _votesToSkip++;
                int total = NAPI.Pools.GetAllPlayers().Count;
                int need = total / 2 + 1;
                need = need - _votesToSkip;
                if (need <= 0)
                {
                    NAPI.Data.SetWorldData("LAST_VOTESKIP_SUCCESS", now);

                    //Master.TransitionState(new RaceCleanupState());
                    foreach (var p in NAPI.Pools.GetAllPlayers())
                    {
                        p.TriggerEvent("CREATE_SKY_CAMERA", p.Position);
                        p.TriggerEvent("HIDE_INSTRUCTIONAL_BUTTONS");
                    }

                    Master.TransitionState(new VotemapState(_race));
                }
                else
                    NAPI.Chat.SendChatMessageToAll($"Player ~b~{player.Name}~w~ has voted to skip the map! ~r~{need}~w~ votes left to change the map!");
            }
            else if (name == "START_NITRO" && args.Length > 0)
            {
                NAPI.Pools.GetAllPlayers().ForEach((p) => p.TriggerEvent("START_VEHICLE_NITRO_EFFECT", args[0]));
            }
            else if(name == "STOP_NITRO" && args.Length > 0)
            {
                
                NAPI.Pools.GetAllPlayers().ForEach((p) => p.TriggerEvent("STOP_VEHICLE_NITRO_EFFECT", args[0]));
            }
        }

        public override void OnUpdate()
        {
            _delayer.Update();
            _placer.Update();
            Checkpoints.Update();

            if (_pendingSpawns.Count > 0 && (DateTime.UtcNow - _raceStart).TotalSeconds >= NO_SPAWN_PERIOD_SECONDS)
            {
                foreach (var p in _pendingSpawns) _playerRespawnQueue.Enqueue(p);

                _pendingSpawns.Clear();
            }

            if ((_firstPlayerFinished.HasValue && (DateTime.UtcNow - _firstPlayerFinished.Value).TotalSeconds > TIMEOUT_AFTER_FIRST_PLAYER_FINISHES_SECONDS) ||
                (_playersInRace.Count == 0))
            {
                Master.TransitionState(new ScoreboardState(_race, _race.Checkpoints[_race.Checkpoints.Length - 1]));
            }

            if (_playerRespawnQueue.Count > 0)
            {
                Client player = _playerRespawnQueue.Dequeue();

                if (!player.HasDataEx("HAS_LOADED") || (bool)player.GetDataEx("HAS_LOADED") == false || !player.HasDataEx("VEHICLE_MODEL"))
                {
                    Console.WriteLine("Placing player {0} at a spawnpoint.", player.Name);
                    PlacePlayer(player, _race.SpawnPoints[0], _race.AvailableVehicles[Util.GetRandInt(_race.AvailableVehicles.Length)]);
                }
                else
                {
                    int passed = (int) player.GetDataEx("CHECKPOINTS_PASSED");
                    int model = (int) player.GetDataEx("VEHICLE_MODEL");
                    bool secondary = player.HasDataEx("PASSED_SECONDARY_CHECKPOINT") && (bool)player.GetDataEx("PASSED_SECONDARY_CHECKPOINT");

                    if (passed > 0)
                    {
                        Vector3 current = _race.Checkpoints[passed - 1];
                        Vector3 next = _race.Checkpoints[passed];

                        if (secondary)
                        {
                            current = _race.SecondaryCheckpoints[passed - 1];
                            if (_race.SecondaryCheckpoints[passed].X != 0)
                                next = _race.SecondaryCheckpoints[passed];
                        }

                        Vector3 dir = next - current;
                        dir = dir.Normalized;

                        double headingRad = -Math.Atan2(dir.X, dir.Y);
                        float heading = (float)(headingRad * 57.2958);

                        Console.WriteLine("Placing player {0} at his last checkpoint.", player.Name);

                        PlacePlayer(player, new SpawnPoint()
                        {
                            Position = current,
                            Heading = heading,
                        }, (VehicleHash)model);
                    }
                    else
                    {

                        Console.WriteLine("Placing player {0} at first checkpoint.", player.Name);
                        PlacePlayer(player, _race.SpawnPoints[0], (VehicleHash)model);
                    }
                    
                }
            }

            if ((DateTime.UtcNow - _lastPositionUpdate).TotalMilliseconds >= POSITION_UPDATE_INTERVAL_MS)
            {
                Dictionary<int, List<Client>> playersInCheckpoints = new Dictionary<int, List<Client>>();
                int total = NAPI.Pools.GetAllPlayers().Count;

                foreach (var player in NAPI.Pools.GetAllPlayers())
                {
                    if (player.HasDataEx("IN_RACE") && (bool)player.GetDataEx("IN_RACE") == false) continue;

                    if (player.HasDataEx("CHECKPOINTS_PASSED"))
                    {
                        int chk = (int)player.GetDataEx("CHECKPOINTS_PASSED");

                        if (!playersInCheckpoints.ContainsKey(chk))
                            playersInCheckpoints.Add(chk, new List<Client>());
                        playersInCheckpoints[chk].Add(player);
                    }
                }

                int[] totalPlayersBelowCheckpoint = new int[_race.Checkpoints.Length + 1];

                for (int i = _race.Checkpoints.Length - 2; i >= 0; i--)
                {
                    int prevLevel = totalPlayersBelowCheckpoint[i + 1];

                    if (playersInCheckpoints.ContainsKey(i + 1))
                        totalPlayersBelowCheckpoint[i] = prevLevel + playersInCheckpoints[i + 1].Count;
                    else totalPlayersBelowCheckpoint[i] = prevLevel;
                }
                

                // Order closest to farthest
                foreach (var pair in playersInCheckpoints)
                {
                    var playersInThisLevel = pair.Value.OrderBy(cl => (cl.Position - _race.Checkpoints[pair.Key]).LengthSquared()).ToArray();

                    for (int i = 0; i < playersInThisLevel.Length; i++)
                    {
                        int position = totalPlayersBelowCheckpoint[pair.Key] + i + 1 + _finishedPlayers.Count;
                        playersInThisLevel[i].SetDataEx("RACE_POSITION", position);
                        playersInThisLevel[i].TriggerEvent("UPDATE_POSITION", position, Util.AddOrdinal(position), pair.Key, _race.Checkpoints.Length);
                    }
                   
                }

            }
        }


        // Private methods

        private void PlayerInsideCheckpoint(Client player, Checkpoints.Checkpoint checkpoint)
        {
            if (!player.HasDataEx("IN_RACE") || (bool)player.GetDataEx("IN_RACE") == false) return;

            if (!player.HasDataEx("CHECKPOINTS_PASSED")) player.SetDataEx("CHECKPOINTS_PASSED", 0);


            int id = checkpoint.Id;
            int playerCheckpointsPassed = (int) player.GetDataEx("CHECKPOINTS_PASSED");

            if (playerCheckpointsPassed == id)
            {
                playerCheckpointsPassed++;

                player.SetDataEx("PASSED_SECONDARY_CHECKPOINT", checkpoint.Secondary);

                // First checkpoint, turn off ghostmode
                if (playerCheckpointsPassed == 1)
                {
                    Vehicle veh = (Vehicle)player.GetDataEx("RACE_VEHICLE");

                    foreach (var p in NAPI.Pools.GetAllPlayers()) p.TriggerEvent("SET_VEHICLE_GHOSTMODE", veh, false);
                }

                if (playerCheckpointsPassed == _race.Checkpoints.Length)
                {
                    _finishedPlayers.Add(player);
                    _playersInRace.Remove(player);

                    player.SetDataEx("IN_RACE", false);
                    player.TriggerEvent("RESET_CHECKPOINTS");
                    player.TriggerEvent("CLEAR_POSITION");
                    player.TriggerEvent("HIDE_INSTRUCTIONAL_BUTTONS");

                    TimeSpan timeElapsed = (DateTime.UtcNow - _raceStart);
                    string timeFormatted = timeElapsed.ToString("mm':'ss'.'fff");

                    player.SetDataEx("RACE_TIME", timeElapsed.TotalMilliseconds);
                    player.SetDataEx("RACE_POSITION", _finishedPlayers.Count);

                    if (!_firstPlayerFinished.HasValue)
                        _firstPlayerFinished = DateTime.UtcNow;

                    NAPI.Chat.SendChatMessageToAll($"Player ~b~{player.Name}~w~ has finished #{_finishedPlayers.Count} with time of {timeFormatted}!");
                }
                else
                {
                    player.SetDataEx("CHECKPOINTS_PASSED", playerCheckpointsPassed);

                    int playerPos = (int)player.GetDataEx("RACE_POSITION");

                    player.TriggerEvent("UPDATE_POSITION", playerPos, Util.AddOrdinal(playerPos), playerCheckpointsPassed, _race.Checkpoints.Length);

                    SendNextCheckpoint(player, playerCheckpointsPassed);

                    player.TriggerEvent("PLAY_CHECKPOINT_SOUND");

                    if (checkpoint.TransformVehicle.HasValue)
                    {
                        int vehicle = checkpoint.TransformVehicle.Value;

                        if (vehicle == 0)
                        {
                            int originalModel = (int) player.GetDataEx("ORIGINAL_VEHICLE_MODEL");
                            ChangePlayerVehicleModel(player, originalModel);
                        }
                        else
                        {
                            ChangePlayerVehicleModel(player, vehicle);
                        }
                    }
                }
            }
        }

        private void SendNextCheckpoint(Client player, int checkpoint)
        {
            string checkpointData = Newtonsoft.Json.JsonConvert.SerializeObject(new
            {
                size = _race.CheckpointSizes != null && _race.CheckpointSizes.Length > 0 ? _race.CheckpointSizes[checkpoint] : 1,
                current = _race.Checkpoints[checkpoint],
                next = _race.Checkpoints.Length > checkpoint + 1 ? _race.Checkpoints[checkpoint + 1] : null,
                secondaryCurrent = _race.SecondaryCheckpoints != null &&
                                   _race.SecondaryCheckpoints.Length > 0 &&
                                   _race.SecondaryCheckpoints[checkpoint].X != 0 ?
                        _race.SecondaryCheckpoints[checkpoint] : null,
                secondaryNext = _race.SecondaryCheckpoints != null &&
                                _race.SecondaryCheckpoints.Length > checkpoint + 1 &&
                                _race.SecondaryCheckpoints[checkpoint + 1].X != 0 ?
                        _race.SecondaryCheckpoints[checkpoint + 1] : null,
                currentFinish = checkpoint == _race.Checkpoints.Length - 1,
                nextFinish = checkpoint == _race.Checkpoints.Length - 2,
                transformCheckpoint = _race.TransformCheckpoints != null &&
                    _race.TransformCheckpoints.Length > 0 &&
                    _race.TransformCheckpoints[checkpoint] > 0
            });

            player.TriggerEvent("SET_NEXT_CHECKPOINT", checkpointData);
        }

        private void ChangePlayerVehicleModel(Client player, int newModel)
        {
            int currentVModel = (int) player.GetDataEx("VEHICLE_MODEL");
            if (currentVModel == newModel) return;

            foreach (var p in NAPI.Pools.GetAllPlayers())
                p.TriggerEvent("CREATE_FX", "scr_rcbarry2", "scr_clown_appears", player.Position, 10.5f);

            player.TriggerEvent("SAVE_VELOCITY");
            player.SetDataEx("VEHICLE_MODEL", newModel);
            
            Vehicle playersCar = (Vehicle)player.GetDataEx("RACE_VEHICLE");

            if (playersCar == null) return;

            NAPI.Entity.SetEntityModel(playersCar, newModel);

            player.TriggerEvent("APPLY_VELOCITY", playersCar);

            playersCar.EngineStatus = true;
        }

        private void PlacePlayer(Client player, SpawnPoint sp, VehicleHash carmodel)
        {
            Console.WriteLine("Placing player w/ model {0}", carmodel);

            if (!player.HasDataEx("ORIGINAL_VEHICLE_MODEL")) player.SetDataEx("ORIGINAL_VEHICLE_MODEL", (int)carmodel);

            foreach (var p in NAPI.Pools.GetAllPlayers())
            {
                p.TriggerEvent("CREATE_FX", "scr_rcbarry2", "scr_clown_appears", player.Position, 10.5f);
                p.TriggerEvent("CREATE_FX", "scr_rcbarry2", "scr_clown_appears", sp.Position, 10.5f);
            }

            _placer.EnqueuePlayer(player, sp, (int)carmodel);            
        }

    }
}
