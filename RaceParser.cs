﻿using GTANetworkAPI;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace ProperCuntingRacingResource
{
    public static class RaceParser
    {
        public struct AvailableMap
        {
            public string Path;
            public string Name;
        }

        public static AvailableMap[] AllMaps;

        public static void DiscoverMaps()
        {
            AllMaps = Directory.GetFiles("race-maps", "*.xml")
                .Select(p => new AvailableMap() { Path = p, Name = Path.GetFileNameWithoutExtension(p) })
                .ToArray();
        }

        public static Race Parse(string path)
        {
            XmlSerializer xml = new XmlSerializer(typeof(Race));

            using (var stream = File.OpenRead(path))
            {
                Race r;
                try
                {
                    r = (Race)xml.Deserialize(stream);
                }
                catch (InvalidOperationException)
                {
                    Console.WriteLine(">>>MAP {0} IS BAD! REMOVE IT", path);

                    throw;
                }

                r.Filename = path;

                for (int i = 0; i < r.SpawnPoints.Length; i++)
                {
                    r.SpawnPoints[i].Position = new Vector3(r.SpawnPoints[i].Position.X, r.SpawnPoints[i].Position.Y, r.SpawnPoints[i].Position.Z + 1f);
                }

                List<VehicleHash> availableCars = r.AvailableVehicles.ToList();
                var grouped = availableCars.GroupBy(v => NAPI.Vehicle.GetVehicleClass(v)).ToArray();
                r.AvailableVehicles = grouped[Util.GetRandInt(grouped.Length)].ToArray();

                return r;
            }
        }
    }

    public static class RangeExtension
    {
        public static bool IsInRangeOf(this Vector3 center, Vector3 dest, float radius)
        {
            return center.Subtract(dest).Length() < radius;
        }

        public static Vector3 Subtract(this Vector3 left, Vector3 right)
        {
            if (left == null || right == null)
            {
                return new Vector3(100, 100, 100);
            }

            return new Vector3()
            {
                X = left.X - right.X,
                Y = left.Y - right.Y,
                Z = left.Z - right.Z,
            };
        }

        public static float Length(this Vector3 vect)
        {
            return (float)Math.Sqrt((vect.X * vect.X) + (vect.Y * vect.Y) + (vect.Z * vect.Z));
        }
    }


    public class Race
    {
        public Vector3[] Checkpoints;
        public SpawnPoint[] SpawnPoints;
        public VehicleHash[] AvailableVehicles;
        public bool LapsAvailable = true;
        public Vector3 Trigger;
        public SavedProp[] DecorativeProps;

        public string Filename;

        public string Name;
        public string Description;
        public string Thumbnail;

        public int[] TransformModels;
        public int[] TransformCheckpoints;

        public float[] CheckpointSizes;
        public Vector3[] SecondaryCheckpoints;

        public Race() { }

        public Race(Race copyFrom)
        {
            Checkpoints = copyFrom.Checkpoints;
            SpawnPoints = copyFrom.SpawnPoints;
            AvailableVehicles = copyFrom.AvailableVehicles;
            LapsAvailable = copyFrom.LapsAvailable;
            Trigger = copyFrom.Trigger;
            DecorativeProps = copyFrom.DecorativeProps;

            Name = copyFrom.Name;
            Description = copyFrom.Description;
        }
    }

    public class SpawnPoint
    {
        public Vector3 Position { get; set; }
        public float Heading { get; set; }
    }

    public class SavedProp
    {
        public Vector3 Position { get; set; }
        public Vector3 Rotation { get; set; }
        public int Hash { get; set; }
        public bool Dynamic { get; set; }
    }

}
