﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

namespace ProperCuntingRacingResource
{
    public static class Util
    {
        //public static Random Random = new Random();
        private static RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider();

        public static int GetRandInt(int max)
        {
            byte[] bytes = new byte[4];
            rngCsp.GetBytes(bytes);
            bytes[3] = (byte) (bytes[3] & 0x7F);

            return BitConverter.ToInt32(bytes, 0) % max;
        }

        public static double GetRand()
        {
            int n = GetRandInt(int.MaxValue);
            return n / (float)int.MaxValue;
        }

        public static void FreezeEntityForPlayer(Client player, Entity entity, bool freeze)
        {
            player.TriggerEvent("FREEZE_ENTITY", entity, freeze);
        }

        public static string AddOrdinal(int num)
        {
            if (num <= 0) return num.ToString();

            switch (num % 100)
            {
                case 11:
                case 12:
                case 13:
                    return num + "th";
            }

            switch (num % 10)
            {
                case 1:
                    return num + "st";
                case 2:
                    return num + "nd";
                case 3:
                    return num + "rd";
                default:
                    return num + "th";
            }

        }

        public class Delayer
        {
            private struct Delay
            {
                public DateTime When;
                public Action Callback;
            }

            private List<Delay> _delays = new List<Delay>();

            public void Update()
            {
                for (int i = _delays.Count - 1; i >= 0; i--)
                {
                    if ((DateTime.UtcNow - _delays[i].When).TotalMilliseconds >= 0)
                    {
                        _delays[i].Callback();
                        _delays.RemoveAt(i);
                    }
                }
            }

            public void DelayAction(int ms, Action act)
            {
                _delays.Add(new Delay()
                {
                    When = DateTime.UtcNow.Add(TimeSpan.FromMilliseconds(ms)),
                    Callback = act,
                });
            }
        }
    }
}
