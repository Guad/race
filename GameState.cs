﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProperCuntingRacingResource
{
    public abstract class GameState
    {
        public abstract string StateName { get; }

        public virtual void EnterState(GameState previous) { }
        public virtual void LeaveState(GameState next) { }


        public virtual void OnCommand(Client player, string cmd, params object[] args) { }
        public virtual void OnRemoteEvent(Client player, string name, object[] args) { }
        public virtual void OnUpdate() { }
        public virtual void OnChatMessage(Client player, string message) { }
        public virtual void OnPlayerConnected(Client player) { }
        public virtual void OnPlayerDisconnected(Client player, DisconnectionType type, string reason) { }
        public virtual void OnPlayerDeath(Client player, Client killer, uint reason) { }
        public virtual void OnPlayerSpawn(Client player) { }
        public virtual void OnPlayerPickup(Client player, Pickup pickup) { }
        public virtual void OnPlayerDamage(Client player, float healthloss, float armorloss) { }
        public virtual void OnPlayerEnterVehicle(Client player, Vehicle vehicle, sbyte seatId) { }
        public virtual void OnPlayerExitVehicle(Client player, Vehicle vehicle) { }
        public virtual void OnPlayerWeaponSwitch(Client player, WeaponHash oldWeapon, WeaponHash newWeapon) { }
        public virtual void OnPlayerDetonateStickies(Client player) { }
        public virtual void OnPlayerEnterColShape(ColShape shape, Client player) { }
        public virtual void OnPlayerExitColShape(ColShape shape, Client player) { }
        public virtual void OnPlayerEnterCheckpoint(Checkpoint checkpoint, Client player) { }
        public virtual void OnPlayerExitCheckpoint(Checkpoint checkpoint, Client player) { }
        public virtual void OnVehicleDamage(Vehicle vehicle, float bodyHealthLoss, float engineHealthLoss) { }
        public virtual void OnVehicleTyreBurst(Vehicle vehicle, int tyreIndex) { }
        public virtual void OnVehicleDeath(Vehicle veh) { }
    }
}
